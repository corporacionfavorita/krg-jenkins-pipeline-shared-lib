def call(currentBuild) {
  echo 'Las ramas diferentes a master y develop se van a abortar'
  if (!(env.BRANCH_NAME == 'develop')) {
    currentBuild.result = 'ABORTED'
    error('Stopping early…')
  }
}
