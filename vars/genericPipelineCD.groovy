import ec.com.smx.Constants

def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()
  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "generic-cd-slave-${projectName}"

  properties([
  durabilityHint('PERFORMANCE_OPTIMIZED')])

  pipeline {
    agent {
      kubernetes {
        defaultContainer 'helm-kubectl'
        label labelgenerated
        yaml """
      apiVersion: v1
      kind: Pod
      spec:
        containers:
        - name: git
          image: alpine/git:v2.24.3
          imagePullPolicy: IfNotPresent
          command:
          - cat
          tty: true
        - name: gradle8
          image: gradle:8.5-jdk21
          command:
          - cat
          tty: true
        - name: gradle4
          image: cf1dck.cfavorita.net/ec.com.smx.jenkins/gradle-agent:1.0.1-RELEASE
          imagePullPolicy: IfNotPresent
          command:
          - cat
          env:
          - name: GRADLE_OPTS
            value: -Xms2g -Xmx4g -XX:MaxHeapSize=3g -Dfile.encoding=UTF-8 -XX:+UseG1GC -XX:+HeapDumpOnOutOfMemoryError
          tty: true
        - name: node14
          image: cf1dck.cfavorita.net/ec.com.smx.jenkins/node14-jnlp-slave:1.0.0-RELEASE
          imagePullPolicy: IfNotPresent
          resources:
            requests:
              memory: "256Mi"
              cpu: "250m"
            limits:
              memory: "4000Mi"
              cpu: "1000m"
          securityContext:
          privileged: true
          command:
          - cat
          tty: true
        - name: node20
          image: node:20
          command:
          - cat
          tty: true
        - name: node18
          image: node:18.13.0
          resources:
            requests:
              memory: "256Mi"
              cpu: "250m"
            limits:
              memory: "4000Mi"
              cpu: "1000m"
          securityContext:
            privileged: true
          command:
          - cat
          tty: true
        - name: python3
          image: cf1dck.cfavorita.net/ec.com.smx.jenkins/python-agent:1.0.4-SNAPSHOT
          imagePullPolicy: Always
          resources:
            requests:
              memory: "256Mi"
              cpu: "250m"
            limits:
              memory: "4000Mi"
              cpu: "1500m"
          securityContext:
          privileged: true
          command:
          - cat
          tty: true
        - name: docker
          image: docker
          command:
          - cat
          tty: true
          volumeMounts:
          - name: docker-sock
            mountPath: /var/run/docker.sock
        - name: sonar-scanner
          image: sonarsource/sonar-scanner-cli:latest
          imagePullPolicy: IfNotPresent
          command:
          - cat
          tty: true
        - name: helm-kubectl
          image: dtzar/helm-kubectl:latest
          imagePullPolicy: IfNotPresent
          command:
          - cat
          tty: true
          volumeMounts:
          - mountPath: /var/run/docker.sock
            name: docker-sock 
        volumes:
        - name: docker-sock
          hostPath:
            path: /var/run/docker.sock
        """
      }
    }
 
    environment {
        registryCredential = 'cf-nexus'
        NXSJAVA_CREDENTIAL = credentials('cf-nexus')
        NXSDOCKER_CREDENTIAL = credentials('cf-nexus')
        NXSHELM_CREDENTIAL = credentials('cf-nexus')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 30, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
          steps { 
            script {
                env.CURRENT_STAGE = Constants.STAGES.CHECK_CURRENT_BUILDS
              }       
              checkCurrentBuilds()
          }
      }

      stage('Checkout code') {
        steps {
          script {
                env.CURRENT_STAGE = Constants.STAGES.CHECKOUT_CODE
            }
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          script {
            env.CURRENT_STAGE = Constants.STAGES.SETUP_ENVIRONMENT

              echo "Tipo de build:  ${pipelineParams.buildType}\n"
              switch(pipelineParams.buildType) {
                case Constants.NPM_BUILD14:
                case Constants.NPM_BUILD20:
                  setupEnviromentNode(pipelineParams.buildType)
                  break
                case Constants.GRADLE_BUILD4:
                case Constants.GRADLE_BUILD8:
                  setupEnviromentGradle(pipelineParams.buildType)
                  break
                case Constants.PYTHON_BUILD:
                  setupEnviromentPython3()
                  break
                default:
                  break
              }
            }
          setupEnviromentGit()

          setupEnviromentDocker()

          gitFlowValidate()

        }
      }

      stage('Install dependencies') {
        steps {
          echo "Iniciando construccion"
          script {
              env.CURRENT_STAGE = Constants.STAGES.BUILD
              echo "Tipo de build:  ${pipelineParams.buildType}\n"
              switch(pipelineParams.buildType) {
                case Constants.NPM_BUILD14:
                case Constants.NPM_BUILD20:
                  container(pipelineParams.buildType) {
                    echo 'Install NPM dependencies'
                    script {
                      try {
                        sh '''
                          npm cache clean --force
                          npm cache verify
                          npm install 
                        '''
                      } catch(err) {
                        sh 'npm install -f --no-package-lock -d --quiet --loglevel=warn'

                      }
                    }
                  }
                  break
                case Constants.GRADLE_BUILD4:
                case Constants.GRADLE_BUILD8:
                  break
                case Constants.PYTHON_BUILD:
                  container('python3') {
                      sh "python -m pip install --upgrade pip"
                      sh "pip install -r requirements.txt"
                      sh "pip install safety"
                  }
                  break
                default:

                  break
              }
            }
        }
      }

      stage('Unit Test') {
        steps {
          script {
                env.CURRENT_STAGE = Constants.STAGES.UNIT_TEST
             echo "Tipo de build:  ${pipelineParams.buildType}\n"
              switch(pipelineParams.buildType) {
                case Constants.NPM_BUILD14:
                case Constants.NPM_BUILD20:
                  container(pipelineParams.buildType) {
                    echo 'Test project'
                    script {
                      try {
                    sh "npm run-script jtest"
                      } catch(err) {
                    unstable('Code Quality - Unit Test failed!')
                      }
                    }
                  }
                  break
                case Constants.GRADLE_BUILD4:
                case Constants.GRADLE_BUILD8:
                  container(pipelineParams.buildType) {
                    sh "gradle test --console=plain -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} --stacktrace"
                  }
                  break
                case Constants.PYTHON_BUILD:
                    container('python3') {
                      sh "python ./test/main.py "
                    }
                  break
                default:

                  break
              }

            }
        }
        post {
          always {
            junit allowEmptyResults: true,
            testResults: '**/test-results.xml'
          }
        }
      }

      stage('Sonarqube') {
        steps {
          script {
                env.CURRENT_STAGE = Constants.STAGES.SONARQUBE
            }
          sonarScanner10()
        }
      }

      stage('Code Quality - Security') {
        steps {
          script {
                env.CURRENT_STAGE = Constants.STAGES.SECURITY
              echo "Tipo de build:  ${pipelineParams.buildType}\n"
              switch(pipelineParams.buildType) {
                case Constants.NPM_BUILD14:
                case Constants.NPM_BUILD20:
                    container(pipelineParams.buildType) {
                    script {
                        try {
                          sh 'npm i kng-utils@latest --save-dev'
                          sh 'node node_modules/kng-utils security'
                        } catch (err) {
                          unstable('Code Quality - Security failed!')
                        }
                    }
                  }
                  break
                case Constants.GRADLE_BUILD4:
                case Constants.GRADLE_BUILD8:
                  container(pipelineParams.buildType) {
                    script {
                      try {
                        sh 'gradle dependencyCheckAnalyze -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}'
                      } catch(err) {
                        unstable('Code Quality - Security failed!')
                      }
                    }
                  }
                  break
                case Constants.PYTHON_BUILD:
                  container('python3') {
                    script {
                      try {
                        sh 'safety check'
                      } catch(err) {
                        unstable('Code Quality - Security failed!')
                      }
                    }
                  }
                  break
                default:

                  break
              }
            }
        }
      }

      stage('Build Docker Image') {
        steps {
          script {
              env.CURRENT_STAGE = Constants.STAGES.DOCKER_BUILD
                        echo "Tipo de build:  ${pipelineParams.buildType}\n"
              switch(pipelineParams.buildType) {
                case Constants.NPM_BUILD14:
                case Constants.NPM_BUILD20:
                    container(pipelineParams.buildType) {
                      echo 'Start build project'
                      def buildActive = pipelineParams.buildActive != null ? pipelineParams.buildActive.toBoolean() : true

                      if(buildActive){
                          sh 'npm run-script build'
                      }
                    }
                    dockerBuildGeneric()
                  break
                case Constants.GRADLE_BUILD4:
                case Constants.GRADLE_BUILD8:
                    dockerBuildGeneric()
                  break
                case Constants.PYTHON_BUILD:
                     container('docker') {
                      echo 'Start build docker'
                      sh 'docker version'
                      sh "docker build --no-cache -t ${env.DOCKER_REGISTRY_IMAGE} -f ci/docker/DockerfileCI ."
                      sh "docker tag ${env.DOCKER_REGISTRY_IMAGE} ${env.DOCKER_REGISTRY_IMAGE_LATEST}"
                    }
                  break
                default:

                  break
              }

          }

        }
      }

      stage('Push Docker Image') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          dockerPush()
        }
      }

      stage('Package helm chart') {
        steps {
           script {
                env.CURRENT_STAGE = Constants.STAGES.PACKAGE_HELM
            }
          helmPackage()
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
           script {
                env.CURRENT_STAGE = Constants.STAGES.PUBLISH_HELM
            }
          helmPush()
        }
      }

    }

    post {
        success {
            script {
                slackSimpleMessage("***************** \n El pipeline se ha ejecutado correctamente \n para el repositorio ${env.REPO_NAME} \n\n¡Buen trabajo equipo! \n ***************** :white_check_mark: ")
            }
        }
        failure {
            script {
                slackSimpleMessage("***************** \n El pipelinese ha ejecutado con error \n para el repositorio ${env.REPO_NAME} \n\n¡Revisar el commit equipo! \n ***************** :x: ")
            }
        }
    }
  }
}

// Función simplificada para enviar mensajes a Slack
def slackSimpleMessage(String message) {
    def slackWebhookUrl = 'https://hooks.slack.com/services/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'

    // Encierra el mensaje en comillas simples para asegurar que el shell lo maneje correctamente
    def payload = "payload={\"text\": \"${message}\", \"mrkdwn\": true}"

    // Usa 'sh' para ejecutar 'curl' y enviar el mensaje a Slack, asegurándote de que todo el comando esté correctamente entrecomillado
    sh "curl -X POST --data-urlencode '${payload}' '${slackWebhookUrl}'"
}

def setupEnviromentNode(String agente) {
  container(agente) {
    script {
      env.VERSION_APP = sh(script: "npm run-script version |tail -1", returnStdout: true).trim()
      env.NAME_APP = sh(script: "npm run-script name |tail -1", returnStdout: true).trim()
      env.SHORT_NAME_APP = env.NAME_APP.substring(env.NAME_APP.lastIndexOf('/') + 1, env.NAME_APP.length())
      env.HAS_SUBPROJECTS = "false"
      sh 'printenv'
    }
  }
}

def setupEnviromentGradle(String agente) {
  container(agente) {
    script {
      env.VERSION_APP = sh(script: "gradle -q printMainVersion -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} | tail -1", returnStdout: true).trim()
      env.NAME_APP = sh(script: "gradle -q printProjectName -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} | tail -1", returnStdout: true).trim()
      env.NAME_APP_LIST = sh(script: "gradle -q printProjectName -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}", returnStdout: true).trim()
      env.PUBLISH_APP_PATHS = sh(script: "gradle -q printDockerProjectNames -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}", returnStdout: true).trim()
      env.HAS_SUBPROJECTS = sh(script: "gradle -q hasDockerSubModules -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}", returnStdout: true).trim()
      env.SHORT_NAME_APP = env.NAME_APP.substring(env.NAME_APP.lastIndexOf('/') + 1, env.NAME_APP.length())

      env.GRADLE_VERSION = sh(script: "gradle --version", returnStdout: true).trim()

      sh 'printenv'
    }
  }
}