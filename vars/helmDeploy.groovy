def call(Map config = [: ]) {
  container('helm-kubectl') {
    //slackNotifyDeclarative(config.slackNotificationChannel, ':smiley::rocket: *VERSIÓN DISPONIBLE* Existe una nueva versión de aplicación disponible')
    slackNotifyStartDeploy(config.slackNotificationChannel, ':smiley::desktop_computer: *DESPLIEGUE AUTOMÁTICO* Se ha comenzado un despliegue automático al ambiente de PRUEBAS')   
    script {
      //try {
        //input(id: 'confirm', message: 'Desea desplegar la aplicación en entorno de QA?', ok: 'Si')
        container('helm-kubectl') {            
          echo 'Start delivery helm chart'
          withKubeConfig([credentialsId: 'kubeconfig']) {
            sh 'kubectl config current-context'
            sh 'kubectl config view'
            sh 'helm dep update ./ci/helm'
//            sh 'helm template ${env.SHORT_NAME_APP} ./ci/helm --debug -n ${config.namespace}'
            sh "helm upgrade --install ${env.SHORT_NAME_APP} ./ci/helm --wait -n ${config.namespace}" // --create-namespace  + in helm 3.2
          }
        }
      /*} catch (org.jenkinsci.plugins.workflow.steps.FlowInterruptedException e) {
          echo "Caught ${e.toString()}"
          currentBuild.result = "SUCCESS"
      }*/
    }
  }
}