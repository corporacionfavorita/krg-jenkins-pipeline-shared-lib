def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()
  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "java8-sb-slave-${projectName}"

  properties([
    durabilityHint('PERFORMANCE_OPTIMIZED') 
  ])

  pipeline {
    agent {
      kubernetes {
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: java8-jnlp-slave
      jenkins/java8-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: gradle
      image: gradle:6.9.1-jdk11
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: GRADLE_OPTS
        value: -Xms2g -Xmx4g
      tty: true
    - name: docker
      image: docker:19.03.8
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: cf-regcred
  """
      }
    }

    environment {
      registryCredential = 'cf-nexus'
      NXSJAVA_CREDENTIAL = credentials('cf-nexus')
      NXSDOCKER_CREDENTIAL = credentials('cf-nexus')
      NXSHELM_CREDENTIAL = credentials('cf-nexus')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 60, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
        steps {
          checkCurrentBuilds()
        }
      }
      
      stage('Checkout code') {
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          setupEnviromentGit()
            
          setupEnviromentGradlew()

          setupEnviromentDocker()
          
          gitFlowValidate()
        }
      }

      stage('Clean') {
        steps {
          container('gradle') {
            echo "Rama: ${env.BRANCH_NAME},  codigo de construccion: ${env.BUILD_ID} en ${env.JENKINS_URL}"
            echo "Iniciando limpieza"
            sh "./gradlew --console=plain clean -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
          }
        }
      }

      stage('Install dependencies - Build') {
        steps {
          echo "Iniciando construccion"
          container('gradle') {
            script {
              sh "java -version"
              sh "gradle -version"
              sh "./gradlew --refresh-dependencies --console=plain build copyLombokToLibOut -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} -x test"
            }
          }
        }
      }

      stage('Unit Test') {
        steps {
          container('gradle') {
            echo 'Test project'
            script {
              try {
                sh "./gradlew test --console=plain -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} --stacktrace"
              } catch(err) {
                unstable('Unit Test - Unit Test failed!')
              }
            }
          }
        }
        post {
          always {
            junit allowEmptyResults: true,
            testResults: '**/test-results.xml'
          }
        }
      }

      stage('Integration Test') {
        steps {
          container('gradle') {
            echo 'Test project'
            script {
              try {
                sh '''
                  ./gradlew --console=plain integrationTest jacocoTestReport -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} --stacktrace
                  '''
              } catch(err) {
                unstable('Integration Test - Integration Test failed!')
              }
            }
          }
        }
        post {
          always {
            junit allowEmptyResults: true,
            testResults: '**/integration-test-results.xml'
          }
        }
      }

      stage('Jacoco Test Report') {
        steps {
          container('gradle') {
            echo 'Test report'
            script {
              try {
                sh "./gradlew --console=plain jacocoTestReport -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
              } catch(err) {
                unstable('Jacoco Test Report - Jacoco Test Report failed!')
              }
            }
          }
        }
      }

      stage('Sonarqube') {
        steps {
          sonarScanner()
        }
      }

      stage('Publish Artifact to Nexus') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          container('gradle') {
            echo "Publish Artifact to Nexus"
            sh "./gradlew upload -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
          }
        }
      }

      stage('Code Quality - Security') {
        steps {
          container('gradle') {
            script {
              try {
                sh './gradlew dependencyCheckAnalyze -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}'
              } catch(err) {
                unstable('Code Quality - Security failed!')
              }
            }
          }
        }
      }

      stage('Code Quality - Helm Lint') {
        steps {
          helmLint()
        }
      }

      stage('Build docker') {
        steps {
          dockerBuild()
        }
      }

      stage('Publish docker image') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          dockerPush()
        }
      }

      stage('Package helm chart') {
        steps {
          helmPackage()
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          helmPush()
        }
      }

      stage('Delivery to qa') {
        when {
          expression {
            (env.PUBLISH.toBoolean() == true) && pipelineParams.enableContinuosDelivery == true
          }
        }
        options {
          timeout(time: 300, unit: 'SECONDS') 
        }                
        steps {
          helmDeploy(slackNotificationChannel: pipelineParams.slackNotificationChannel, namespace: pipelineParams.namespace)
        }
      }

      /*stage('Deploy to prod' ) {
        when {
          expression { env.BRANCH_NAME == 'master' }
        }
        steps{
          container('helm-kubectl') {
            echo 'Start deploy helm chart'
            withKubeConfig([credentialsId: 'k8s-cf-preproduccion', serverUrl:"https://rancherpre.favorita.ec/k8s/clusters/c-mw5ln" ]) {
              sh 'kubectl config current-context'
              sh 'kubectl config view'
              sh 'helm list --all-namespaces'
              sh "helm upgrade --install ${env.SHORT_NAME_APP} ./ci/helm --wait -n ${pipelineParams.namespace} --force"  // --create-namespace  + in helm 3.2
            }
          }
        }  
      }*/

    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      aborted {
		    slackNotifyAborted(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}
