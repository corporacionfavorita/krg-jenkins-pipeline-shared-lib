def call(Map config = [: ]) {
  container('git') {
    script {
      // abortAllPreviousBuildInProgress(currentBuild)
      env.GIT_URL = sh(returnStdout: true, script: "git config --get remote.origin.url").trim()
      env.GIT_REPO_NAME = "${env.GIT_URL}".tokenize('/').last().split("\\.")[0]
      env.GIT_SHORT_COMMIT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
      env.GIT_COMMIT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%H'").trim()
      env.GIT_COMMIT_MSG = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%s'").trim()
      
      if (env.CHANGE_ID == null) {
        env.GIT_USER_NAME = sh(script: "git show -s --pretty='%an' ${env.GIT_COMMIT}", returnStdout: true).trim()
        env.GIT_USER_EMAIL = sh(script: "git --no-pager show -s --format='%ae' ${env.GIT_COMMIT}", returnStdout: true).trim()
      }

      env.BRANCH_PREFIX = "${env.BRANCH_NAME}".tokenize('/').first()
      // Added for first config
      // abortedBranchIfNotMain(currentBuild)
      sh 'printenv'
    }
  }
}