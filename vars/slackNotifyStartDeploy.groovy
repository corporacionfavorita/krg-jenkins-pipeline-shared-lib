def call(String slackNotificationChannel = 'devops', String message = ':simple_smile::computer: *ÉXITO EN CONSTRUCCIÓN*') {
  slackSend(
    iconEmoji: ":rocket",
    channel: slackNotificationChannel,
    color: "#27D4EF",
    message: "${message} para la construcción ${currentBuild.fullDisplayName}\n *Repositorio:* `${env.GIT_REPO_NAME}`\n *Rama:* `${env.BRANCH_NAME}`\n *Autor:* ${env.GIT_USER_NAME} [${env.GIT_USER_EMAIL}]\n *Commit:* ${env.GIT_COMMIT}\n *Construcción:* ${env.BUILD_NUMBER}| <${env.BUILD_URL}|Revisar>"
  )
}
