def call(Map config = [: ]) {
  container('docker') {
    script {
      echo 'Init dockerPush'
      docker.withRegistry('https://' + env.DOCKER_REGISTRY, registryCredential) {
        echo 'Start publish image docker to registry'
        if (env.HAS_SUBPROJECTS == "false") {
          sh "docker push ${env.DOCKER_REGISTRY_IMAGE}"
          //sh "docker push ${env.DOCKER_REGISTRY_IMAGE_LATEST}"
        } else {
          def projectDirs = env.PUBLISH_APP_PATHS.split('\n')
          def nameAppsList = env.NAME_APP_LIST.split('\n')
          for (i = 0; i < projectDirs.length; i++) {
            echo "Publish docker sub ${projectDirs[i]}"
            def subProjectDir = "${projectDirs[i]}"
            def subNameApp = "${nameAppsList[i]}"
            def dockerRegistryImage = env.DOCKER_REGISTRY + '/' + subNameApp.replace('@', '') + ':' + env.VERSION_APP
            def dockerRegistryImageLastest = env.DOCKER_REGISTRY + '/' + subNameApp.replace('@', '') + ':latest'

            sh "docker push ${dockerRegistryImage}"
            //sh "docker push ${dockerRegistryImageLastest}"
          }
        }
      }
    }
  }
}