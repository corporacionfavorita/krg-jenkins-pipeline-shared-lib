def call(Map config = [: ]) {
  container('sonar-scanner') {
    echo 'Code quality'
    withSonarQubeEnv('sonarqube') {
      script {
        try {
          sh "sonar-scanner -Dsonar.projectKey=${env.GIT_REPO_NAME} -Dsonar.projectName=${env.GIT_REPO_NAME} -Dsonar.projectVersion=${env.VERSION_APP} -Dsonar.branch.name=${env.BRANCH_NAME} -Dsonar.qualitygate.wait=true --debug"
        } catch(err) {
          unstable('Sonarqube report - failed!')
        }
      }
    }
  }
}