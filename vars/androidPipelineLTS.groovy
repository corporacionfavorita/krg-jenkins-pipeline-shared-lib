def call(body) {
  def pipelineParams = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()

  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "android-slave-${projectName}"

  properties([
            durabilityHint('MAX_SURVIVABILITY')
    ])

  pipeline {
    agent {
      kubernetes {
        defaultContainer 'android'
        label labelgenerated
        yaml '''
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: android-jnlp-slave
      jenkins/angular-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: android
      image: cf1dck.cfavorita.net/ec.com.smx.jenkins/android-agent:1.0.0-RELEASE
      imagePullPolicy: Always
      securityContext:
      privileged: true
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    imagePullSecrets:
    - name: cf-regcred
  '''
      }
    }

    environment {
      NXSJAVA_CREDENTIAL = credentials('cf-nexus')
      ANDROID_CREDENTIAL = credentials('cf-android-keystore')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 20, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
        steps {
          checkCurrentBuilds()
        }
      }

      stage('Checkout code') {
        steps {
          sh 'printenv'
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up enviroment') {
        steps {
          setupEnviromentGit()

          container('android') {
            script {
              sh 'chmod +x gradlew'
              sh "./gradlew -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} -q printMainVersion"
              env.VERSION_APP = sh(script: "./gradlew -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} -q printMainVersion | tail -1", returnStdout: true).trim()
            }
          }
        }
      }

      stage('Clean') {
        steps {
          container('android') {
            script {
              sh "./gradlew -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} --console=plain clean -x test -x check"
            }
          }
        }
      }

      stage('Install dependencies') {
        environment {
          signingConfigs_alias = "${env.ANDROID_CREDENTIAL_USR}"
          signingConfigs_keyPassword = "${env.ANDROID_CREDENTIAL_PSW}"
          signingConfigs_storePassword = "${env.ANDROID_CREDENTIAL_PSW}"
          signingConfigs_file = '/opt/android-sdk-linux/kruger.keystore.jks'
        }
        steps {
          container('android') {
            script {
              echo 'Install dependencies'
              sh 'printenv'
              sh '''
                ./gradlew -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} --refresh-dependencies build -x test -x check
              '''
            }
          }
        }
      }

      stage('Test/QC') {
        parallel {
          stage('Test') {
            steps {
              container('android') {
                script {
                  echo 'Test project'
                  sh '''
                    ./gradlew -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} --console=plain test --stacktrace
                  '''
                }
              }
            }
            post {
              always {
                junit allowEmptyResults: true, testResults: '**/TEST-*.xml'
              }
            }
          }

          stage('Code Quality - Lint') {
            steps {
              container('android') {
                script {
                  try {
                    echo 'Code quality'
                    sh './gradlew -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} lint'
                  } catch (err) {
                    unstable('Code Quality - Lint failed!')
                  }
                }
              }
            }
            post {
              always {
                junit allowEmptyResults: true, testResults: '**/lint-results.xml'
              }
            }
          }

          stage('Code Quality - Sonarqube 10') {
            steps {
              sonarScanner10()
            }
          }
        }
      }

      stage('Build') {
        environment {
          signingConfigs_alias = "${env.ANDROID_CREDENTIAL_USR}"
          signingConfigs_keyPassword = "${env.ANDROID_CREDENTIAL_PSW}"
          signingConfigs_storePassword = "${env.ANDROID_CREDENTIAL_PSW}"
          signingConfigs_file = '/opt/android-sdk-linux/kruger.keystore.jks'
        }
        steps {
          container('android') {
            script {
              echo 'Start build project'
              sh './gradlew -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} --console=plain assemble'
            }
          }
        }
      }

        //            stage('Deploy') {
        //                when {
        //                    // Only execute this stage when building from the `beta` branch
        //                    branch 'beta'
        //                }
        //                environment {
        //                    // SIGNING_KEYSTORE = credentials('my-app-signing-keystore')
        //                    // SIGNING_KEY_PASSWORD = credentials('my-app-signing-password')
        //                    SIGNING_KEYSTORE = ''
        //                    SIGNING_KEY_PASSWORD = ''
        //                }
        //                steps {
        //                    // Archive the APKs so that they can be downloaded from Jenkins
        //                    archiveArtifacts '**/*.apk'
        //
        //                    // Upload the APK to Google Play
        //                    androidApkUpload googleCredentialsId: 'Google Play', apkFilesPattern: '**/*-release.apk', trackName: 'beta'
        //                }
        //            }
    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      aborted {
		    slackNotifyAborted(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}
