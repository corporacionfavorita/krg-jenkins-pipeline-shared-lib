def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()
  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "node-slave-${projectName}"

  properties([
    durabilityHint('PERFORMANCE_OPTIMIZED')
  ])

  pipeline {
    agent {
      kubernetes {
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: nodejs-jnlp-slave
      jenkins/angular-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      resources:
        requests:
          memory: "32Mi"
          cpu: "10m"
        limits:
          memory: "4000Mi"
          cpu: "100m"
      command:
      - cat
      tty: true
    - name: nodejs
      image: docker.krugernetes.com/ec.com.kruger.jenkinsci/angular8-jnlp-slave:1.2.0
      imagePullPolicy: IfNotPresent
      resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "4000Mi"
          cpu: "1000m"
      securityContext:
      privileged: true
      command:
      - cat
      env:
      - name: http_proxy
        value: pxy12:8080
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080 
      tty: true  
    - name: docker
      image: docker:latest
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: cf-regcred  
  """
      }
    }

    environment {
      registryCredential = 'cf-nexus'
      DOCKER_CREDENTIAL = credentials('cf-nexus')
      NEXUS_CREDENTIAL = credentials('cf-nexus')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 15, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
        steps {
          checkCurrentBuilds()
        }
      }
      
      stage('Checkout code') {
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          container('git') {
            script {
              abortAllPreviousBuildInProgress(currentBuild)

              env.GIT_URL = sh(returnStdout: true, script: "git config --get remote.origin.url").trim()
              env.GIT_REPO_NAME = "${env.GIT_URL}".tokenize('/').last().split("\\.")[0]
              env.GIT_SHORT_COMMIT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
              env.GIT_COMMIT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%H'").trim()
              env.GIT_COMMIT_MSG = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%s'").trim()
              container('git') {
            script {
              env.HAS_SUBPROJECTS = "false"
            }
          }

              if (env.CHANGE_ID == null) {
                env.GIT_USER_NAME = sh(script: "git show -s --pretty='%an' ${env.GIT_COMMIT}", returnStdout: true).trim()
                env.GIT_USER_EMAIL = sh(script: "git --no-pager show -s --format='%ae' ${env.GIT_COMMIT}", returnStdout: true).trim()
              }
              echo "Usuario: ${env.GIT_USER_NAME}"
              echo "Email: ${env.GIT_USER_EMAIL}"

              env.BRANCH_PREFIX = "${env.BRANCH_NAME}".tokenize('/').first()
            }
          }
          container('nodejs') {
            script {
              env.VERSION_APP = sh(script: "npm run-script version |tail -1", returnStdout: true).trim()
              env.NAME_APP = sh(script: "npm run-script name |tail -1", returnStdout: true).trim()
              env.SHORT_NAME_APP = env.NAME_APP.substring(env.NAME_APP.lastIndexOf('/') + 1, env.NAME_APP.length())
              env.DOCKER_REGISTRY = 'cf1dck.cfavorita.net'
              env.DOCKER_REGISTRY_IMAGE = env.DOCKER_REGISTRY + '/' + env.NAME_APP.replace('@', '') + ':' + env.VERSION_APP
              env.DOCKER_REGISTRY_IMAGE_LATEST = env.DOCKER_REGISTRY + '/' + env.NAME_APP.replace('@', '') + ':latest'
              sh 'printenv'
            }
          }
        }
      }

      stage('Install dependencies') {
        steps {
          container('nodejs') {
            echo 'Install NPM dependencies'
            script {
              try {
                sh '''
                  npm cache clean --force
                  npm cache verify
                  npm install -f -d --quiet --loglevel=warn
                '''
              } catch(err) {
                sh 'npm install -f --no-package-lock -d --quiet --loglevel=warn'

              }
            }
          }
        }
      }

      stage('Test/QC') {
        parallel {
          stage('Code Quality - Sonarqube') {
            steps {
              container('nodejs') {
                echo 'Code quality'
                sh "npm run-script cover"
              }
              container('sonar-scanner') {
                echo 'Code quality'
                sonarScanner10()
              }
            }
          }
        }
      }

      stage('Build') {
        steps {
          container('nodejs') {
            echo 'Start build project'
            sh 'npm run-script build'
          }
        }
      }

      stage('Publish') {
        steps {
          container('nodejs') {
            echo 'Start publish project'
            sh 'npm run-script publish'
          }
        }
      }

    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}