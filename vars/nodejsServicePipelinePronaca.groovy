def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()

  properties([
    durabilityHint('PERFORMANCE_OPTIMIZED')
  ])

  pipeline {
    agent {
      kubernetes {
        label 'angular-slave'
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: angular8-jnlp-slave
      jenkins/angular-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: nodejs
      image: docker.krugernetes.com/ec.com.kruger.jenkinsci/angular8-jnlp-slave:1.2.0
      imagePullPolicy: IfNotPresent
      securityContext:
      privileged: true
      command:
      - cat
      tty: true
    - name: docker
      image: docker:latest
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: regdock  
  """
      }
    }

    environment {
      registryCredential = 'nexuskruger'
      JAVA_HOME = tool 'jdk8'
      scannerHome = tool 'SonarScanner 4.2.0';
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 15, unit: 'MINUTES')
    }

    stages {
      stage('Checkout code') {
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          container('git') {
            script {
              abortAllPreviousBuildInProgress(currentBuild)

              env.GIT_URL = sh(returnStdout: true, script: "git config --get remote.origin.url").trim()
              env.GIT_REPO_NAME = "${env.GIT_URL}".tokenize('/').last().split("\\.")[0]
              env.GIT_SHORT_COMMIT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
              env.GIT_COMMIT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%H'").trim()
              env.GIT_COMMIT_MSG = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%s'").trim()

              if (env.CHANGE_ID == null) {
                env.GIT_USER_NAME = sh(script: "git show -s --pretty='%an' ${env.GIT_COMMIT}", returnStdout: true).trim()
                env.GIT_USER_EMAIL = sh(script: "git --no-pager show -s --format='%ae' ${env.GIT_COMMIT}", returnStdout: true).trim()
              }
              echo "Usuario: ${env.GIT_USER_NAME}"
              echo "Email: ${env.GIT_USER_EMAIL}"

            }
          }
          container('nodejs') {
            script {
              env.VERSION_APP = sh(script: "npm run-script version |tail -1", returnStdout: true).trim()
              env.NAME_APP = sh(script: "npm run-script name |tail -1", returnStdout: true).trim()
              env.SHORT_NAME_APP = env.NAME_APP.substring(env.NAME_APP.lastIndexOf('/') + 1, env.NAME_APP.length())
              env.DOCKER_REGISTRY = sh(script: "npm run-script dockerRegistry |tail -1", returnStdout: true).trim()
              env.DOCKER_REGISTRY_IMAGE = env.DOCKER_REGISTRY + '/' + env.NAME_APP.replace('@', '') + ':' + env.VERSION_APP
              env.DOCKER_REGISTRY_IMAGE_LATEST = env.DOCKER_REGISTRY + '/' + env.NAME_APP.replace('@', '') + ':latest'

              echo "docker registry: ${env.DOCKER_REGISTRY}"
              echo "docker registry image: ${env.DOCKER_REGISTRY_IMAGE}"
              echo "docker registry image latest: ${env.DOCKER_REGISTRY_IMAGE_LATEST}"

              sh 'printenv'
            }
          }
        }
      }

      stage('Install dependencies') {
        steps {
          container('nodejs') {
            echo 'Install NPM dependencies'
            sh '''
                npm install -f --no-package-lock -d --quiet --loglevel=warn
            '''
          }
        }
      }

      stage('Test/QC') {
        parallel {
          stage('Unit Test') {
            steps {
              container('nodejs') {
                echo 'Test project'
                sh '''
                npm run-script test:unit
              '''
              }
            }
            post {
              always {
                junit allowEmptyResults: true, testResults: '**/test-results.xml'
              }
            }
          }

          stage('Code Quality - Lint') {
            steps {
              container('nodejs') {
                echo 'Code quality'
                sh 'npm run-script lint:ci'
              }
            }
            post {
              always {
                recordIssues enabledForFailure: true, aggregatingResults: true, tool: checkStyle(pattern: 'checkstyle-result.xml')
              }
            }
          }

          stage('Code Quality - Sonarqube') {
            steps {
              container('nodejs') {
                echo 'Code quality'
                withSonarQubeEnv('SonarQubeKruger') {
                  sh "npm run-script build:all && npm run-script cover && ${scannerHome}/bin/sonar-scanner"
                }
              }
            }
          }

          stage('Code Quality - Security') {
            steps {
              container('nodejs') {
                script {
                  try {
                    sh 'npm run-script test:security'
                  } catch(err) {
                    unstable('Code Quality - Security failed!')
                  }
                }
              }
            }
          }

          stage('Code Quality - helm lint') {
            steps {
              container('helm-kubectl') {
                echo 'Start lint helm chart'
                withKubeConfig([credentialsId: 'kruger-devops-k8s', ]) {
                  sh 'helm lint --strict ./ci/helm'
                }
              }
            }
          }

        }
      }

      stage('Build') {
        steps {
          container('nodejs') {
            echo 'Start build project'
            sh 'npm run-script build:ci'
          }
        }
      }

      stage('Integration Test') {
        steps {
          container('nodejs') {
            echo 'Test project'
            sh '''
            npm run-script test:integration
          '''
          }
        }
      }

      stage('Build docker') {
        steps {
          container('docker') {
            echo 'Start build docker'
            sh 'docker version'
            sh "docker build --network=host -t ${env.DOCKER_REGISTRY_IMAGE} --file ./ci/docker/DockerfileCI ."
            sh "docker tag ${env.DOCKER_REGISTRY_IMAGE} ${env.DOCKER_REGISTRY_IMAGE_LATEST}"
          }
        }
      }

      stage('Publish docker image') {
        when {
          expression {
            env.BRANCH_NAME == 'develop' || env.BRANCH_NAME == 'master'
          }
        }
        steps {
          container('docker') {
            script {
              docker.withRegistry('https://' + env.DOCKER_REGISTRY, registryCredential) {
                echo 'Start publish image docker to registry'
                sh "docker push ${env.DOCKER_REGISTRY_IMAGE}"
                sh "docker push ${env.DOCKER_REGISTRY_IMAGE_LATEST}"
              }
            }
          }
        }
      }

      stage('Package helm chart') {
        when {
          expression {
            env.BRANCH_NAME == 'develop' || env.BRANCH_NAME == 'master'
          }
        }
        steps {
          container('helm-kubectl') {
            echo 'Start build helm chart'
            sh 'helm repo add helm-kruger https://nexus.krugernetes.com/repository/helm/ --username admin --password Password01'
            retry(3) {
              sh 'helm dep update ./ci/helm'
            }
            sh 'helm package ./ci/helm'
          }
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.BRANCH_NAME == 'develop' || env.BRANCH_NAME == 'master'
          }
        }
        steps {
          container('helm-kubectl') {
            echo 'Start build helm chart'
            sh 'helm repo add helm-kruger https://nexus.krugernetes.com/repository/helm/ --username admin --password Password01'
            sh 'helm plugin install --version master https://github.com/sonatype-nexus-community/helm-nexus-push.git'

            retry(3) {
              sh 'helm nexus-push helm-kruger ./ci/helm  -u admin -p Password01'
            }
          }
        }
      }

      stage('Delivery to qa') {
        when {
          expression {
            env.BRANCH_NAME == 'develop'
          }
        }
        steps {
          container('helm-kubectl') {
            echo 'Start delivery helm chart'
            withKubeConfig([credentialsId: 'kruger-devops-k8s', ]) {
              sh 'kubectl config current-context'
              sh "helm upgrade --install ${env.SHORT_NAME_APP} ./ci/helm --wait -n ${pipelineParams.namespace} --force" // --create-namespace  + in helm 3.2
            }
          }
        }
      }

      stage('Deploy to prod') {
        when {
          expression {
            env.BRANCH_NAME == 'master'
          }
        }
        steps {
          container('helm-kubectl') {
            echo 'Start deploy helm chart'
            withKubeConfig([credentialsId: 'kruger-devops-k8s', ]) {
              sh 'kubectl config current-context'
              sh "helm upgrade --install ${env.SHORT_NAME_APP} ./ci/helm --wait -n ${pipelineParams.namespace} --force" // --create-namespace  + in helm 3.2
            }
          }
        }
      }

      /*
      stage('Katalon test'){
        when {
          expression { env.BRANCH_NAME == 'develop' || env.BRANCH_NAME == 'master' }
        }
        steps {
          container('katalon') {
            script {
              sh "pwd"
              if (fileExists('end-to-end')) {
                sh "pwd"
                echo "test version exec: ${env.VERSION_TAG}"
                echo 'Start katalon'
                sh 'katalonc.sh -projectPath="./end-to-end" -browserType="Firefox" -retry=0 -statusDelay=15 -testSuitePath="Test Suites/test" -apiKey="ae74a191-2cb0-4cf0-a61e-1b1d4ffd5774" -sendMail=caguilar@ec.krugercorp.com'
              } else {
                echo 'No exists directory'
              }
            }
          }
        }
      }
*/

    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}