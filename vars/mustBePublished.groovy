def call(Map config = [: ]) {
  when {
    expression {
      env.PUBLISH.toBoolean() == true
    }
  }
}