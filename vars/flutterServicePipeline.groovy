
def call(body) {
  def pipelineParams = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()

   properties([
          //buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '10')),
          durabilityHint('MAX_SURVIVABILITY'),
         //parameters([string(defaultValue: '', description: '', name: 'run_stages')])
  ])


  pipeline {
    //containerTemplate(name: 'jnlp', image: 'jenkins/jnlp-slave:3.35-5-alpine', args: '${computer.jnlpmac} ${computer.name}'),
    agent {
      kubernetes {
        defaultContainer 'nodejs'
        label 'flutter-slave'
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: flutter-jnlp-slave
      jenkins/flutter-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: flutter
      image: cf1dck.cfavorita.net/flutter/kruger-flutter2:1.0.0
      imagePullPolicy: Always
      securityContext:
      privileged: true
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: http://pxy12:8080
      - name: https_proxy
        value: http://pxy12:8080 
      tty: true
    - name: nodejs
      image: docker.krugernetes.com/ec.com.kruger.jenkinsci/angular8-jnlp-slave:1.2.0
      imagePullPolicy: IfNotPresent
      securityContext:
      privileged: true
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: http://pxy12:8080
      - name: https_proxy
        value: http://pxy12:8080 
      tty: true  
    - name: docker
      image: docker:19.03.8
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: http://pxy12:8080
      - name: https_proxy
        value: http://pxy12:8080
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: cf-regcred
    - name: cf-reg-cred
  """
      }
    }

    environment {
      registryCredential = 'nexuskruger'
      DOCKER_CREDENTIAL = credentials('nexuskruger')
    }

    triggers {
      bitbucketPush() 
    }
    
    options {
      disableConcurrentBuilds()
      //skipStagesAfterUnstable()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 30, unit: 'MINUTES')
    }

    stages {
      stage('Checkout code') {      
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }
      
      stage('Set up environment') {
        steps {
          container('git') {
            script {
              abortAllPreviousBuildInProgress(currentBuild)

              env.GIT_URL = sh(returnStdout: true, script: "git config --get remote.origin.url").trim()
              env.GIT_REPO_NAME = "${env.GIT_URL}".tokenize('/').last().split("\\.")[0]
              env.GIT_SHORT_COMMIT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%h'").trim()
              env.GIT_COMMIT = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%H'").trim()
              env.GIT_COMMIT_MSG = sh(returnStdout: true, script: "git log -n 1 --pretty=format:'%s'").trim()
              
              if (env.CHANGE_ID == null){
                env.GIT_USER_NAME = sh(script: "git show -s --pretty='%an' ${env.GIT_COMMIT}", returnStdout: true).trim()
                env.GIT_USER_EMAIL = sh(script: "git --no-pager show -s --format='%ae' ${env.GIT_COMMIT}", returnStdout: true).trim()
              }
              echo "Usuario: ${env.GIT_USER_NAME}"
              echo "Email: ${env.GIT_USER_EMAIL}"

              env.BRANCH_PREFIX = "${env.BRANCH_NAME}".tokenize('/').first()
            }
          }

          container('nodejs') {
            script {
             env.VERSION_APP = sh(script: "npm run-script version |tail -1", returnStdout: true).trim()
              env.NAME_APP = sh(script: "npm run-script name |tail -1", returnStdout: true).trim()
              env.SHORT_NAME_APP =  env.NAME_APP.substring(env.NAME_APP.lastIndexOf('/')+1, env.NAME_APP.length())
              env.DOCKER_REGISTRY = 'cf1dck.cfavorita.net'
              env.DOCKER_REGISTRY_IMAGE = env.DOCKER_REGISTRY +'/'+ env.NAME_APP.replace('@','') +':'+ env.VERSION_APP
              env.DOCKER_REGISTRY_IMAGE_LATEST = env.DOCKER_REGISTRY +'/'+ env.NAME_APP.replace('@','') +':latest'
            }
          }

          container('helm-kubectl') {
            script {
              retry(3) {
                sh 'helm dep update ./ci/helm'
              }
              sh 'helm inspect chart ./ci/helm'
              env.CHART_STDOUT = sh(script: "helm package ./ci/helm | tail -1", returnStdout: true).trim()
              env.CHART_PACKAGE = "${env.CHART_STDOUT}".tokenize('/').last()
              env.LABEL_CHART_PACKAGE = "${env.CHART_PACKAGE}".tokenize('-').last().split("\\.")[0]
              sh 'printenv'
            }
          }
        }
      }

      stage('Install dependencies') {
        steps {
          container('flutter') {
            echo 'Install dependencies'
            sh '''
                flutter pub get
            '''
          }
        }
      }

      stage('Build') {
        steps {
          container('flutter') { 
            echo 'Start build project'
            sh 'flutter build web'
          }
        }
      }

      stage('Build docker') {
        steps {
          container('docker') { 
            echo 'Start build docker'
            sh 'docker version'
            sh "docker build --network=host -t ${env.DOCKER_REGISTRY_IMAGE} --file ./ci/docker/DockerfileCI ."
            sh "docker tag ${env.DOCKER_REGISTRY_IMAGE} ${env.DOCKER_REGISTRY_IMAGE_LATEST}"
          }
        }
      }    

      stage('Publish docker image') {
        steps {
          container('docker') { 
            script {
              docker.withRegistry('https://cf1dck.cfavorita.net/v2/', registryCredential ) {
                echo 'Start publish image docker to registry'
                sh "docker push ${env.DOCKER_REGISTRY_IMAGE}"
                sh "docker push ${env.DOCKER_REGISTRY_IMAGE_LATEST}"
              }
            }
          }
        }
      }

      stage('Package helm chart') {
        steps{
          container('helm-kubectl') {
            echo 'Start build helm chart'
              sh "helm repo add helm-cf https://cf1nxs.favorita.ec/repository/helm/ --username ${env.DOCKER_CREDENTIAL_USR} --password ${env.DOCKER_CREDENTIAL_PSW}"
              retry(3) {
                sh 'helm dep update ./ci/helm'
              }
              sh 'helm package ./ci/helm'
          }
        }  
      }

      stage('Publish helm chart') {
        steps{
          container('helm-kubectl') {
            script {
              echo 'Start publish helm chart'
              if (env.LABEL_CHART_PACKAGE == "release") {
                sh "helm repo add helm-cf https://cf1hlmnxs.cfavorita.net/repository/helm-releases/ --username ${env.DOCKER_CREDENTIAL_USR} --password ${env.DOCKER_CREDENTIAL_PSW}"
              } else {
                sh "helm repo add helm-cf https://cf1hlmnxs.cfavorita.net/repository/helm-snapshots/ --username ${env.DOCKER_CREDENTIAL_USR} --password ${env.DOCKER_CREDENTIAL_PSW}"
              }
              sh 'helm plugin install --version master https://github.com/sonatype-nexus-community/helm-nexus-push.git'

              retry(3) {
                sh "helm nexus-push helm-cf ./ci/helm  -u ${env.DOCKER_CREDENTIAL_USR} -p ${env.DOCKER_CREDENTIAL_PSW}"
              }
            } 
          }
        }  
      }
    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }        
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      changed {
          echo 'This will run only if the state of the Pipeline has changed'
          echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }  
}
