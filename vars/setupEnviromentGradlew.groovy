def call(Map config = [: ]) {
  container('gradle') {
    script {
      sh "chmod +x gradlew"
      env.VERSION_APP = sh(script: "./gradlew -q printMainVersion -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} | tail -1", returnStdout: true).trim()
      env.NAME_APP = sh(script: "./gradlew -q printProjectName -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} | tail -1", returnStdout: true).trim()
      env.NAME_APP_LIST = sh(script: "./gradlew -q printProjectName -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}", returnStdout: true).trim()
      env.PUBLISH_APP_PATHS = sh(script: "./gradlew -q printDockerProjectNames -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}", returnStdout: true).trim()
      env.HAS_SUBPROJECTS = sh(script: "./gradlew -q hasDockerSubModules -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}", returnStdout: true).trim()
      env.SHORT_NAME_APP = env.NAME_APP.substring(env.NAME_APP.lastIndexOf('/') + 1, env.NAME_APP.length())      
      env.GRADLE_VERSION = sh(script: "./gradlew --version", returnStdout: true).trim()
      
      sh 'printenv'
    }
  }
}