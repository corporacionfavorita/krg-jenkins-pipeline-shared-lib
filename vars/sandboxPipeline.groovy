def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()
  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "java8-sb-slave-${projectName}"

  properties([
    durabilityHint('PERFORMANCE_OPTIMIZED') 
  ])

  pipeline {
    agent {
      kubernetes {
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: java8-jnlp-slave
      jenkins/java8-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080 
      tty: true
    - name: gradle
      image: cf1dck.cfavorita.net/jdk/gradle:4.10-jdk-alpine
      imagePullPolicy: IfNotPresent
      command:
      - cat
      volumeMounts:
      - mountPath: "/home/gradle/data"
        name: "gradle-pv-storage"
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080
      - name: GRADLE_OPTS
        value: -Xms2g -Xmx4g -XX:MaxHeapSize=3g -Dfile.encoding=UTF-8 -XX:+UseG1GC -XX:+HeapDumpOnOutOfMemoryError
      tty: true
    - name: docker
      image: docker:19.03.8
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: HTTP_PROXY
        value: http://pxy12:8080
      - name: HTTPS_PROXY
        value: http://pxy12:8080
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
      - name: gradle-pv-storage
        persistentVolumeClaim:
          claimName: "gradle-pv-nfs-claim-spring"
          readOnly: false
    imagePullSecrets:
    - name: cf-regcred
    - name: cf-reg-cred
  """
      }
    }

    environment {
      registryCredential = 'cf-nexus'
      DOCKER_CREDENTIAL = credentials('cf-nexus')
      NEXUS_CREDENTIAL = credentials('cf-nexus')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 40, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
        steps {
          checkCurrentBuilds()
        }
      }
      
      stage('Checkout code') {
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          container('git') {
            script {
              setupEnviromentGit()              
            }
          }

          container('gradle') {
            script {
              env.VERSION_APP = sh(script: "gradle -q printMainVersion | tail -1", returnStdout: true).trim()
              env.NAME_APP = sh(script: "gradle -q printProjectName | tail -1", returnStdout: true).trim()
              env.NAME_APP_LIST = sh(script: "gradle -q printProjectName", returnStdout: true).trim()
              env.PUBLISH_APP_PATHS = sh(script: "gradle -q printDockerProjectNames", returnStdout: true).trim()
              env.HAS_SUBPROJECTS = sh(script: "gradle -q hasDockerSubModules", returnStdout: true).trim()
              env.SHORT_NAME_APP = env.NAME_APP.substring(env.NAME_APP.lastIndexOf('/') + 1, env.NAME_APP.length())
              env.DOCKER_REGISTRY = sh(script: "gradle -q printRegistry | tail -1", returnStdout: true).trim()
              env.DOCKER_REGISTRY_IMAGE = env.DOCKER_REGISTRY + '/' + env.NAME_APP.replace('@', '') + ':' + env.VERSION_APP
              env.DOCKER_REGISTRY_IMAGE_LATEST = env.DOCKER_REGISTRY + '/' + env.NAME_APP.replace('@', '') + ':latest'
            }
          }
          gitFlowValidate()
        }
      }

      stage('Clean') {
        steps {
          container('gradle') {
            echo "Executor: ${env.EXECUTOR_NUMBER}"
            echo "Rama: ${env.BRANCH_NAME},  codigo de construccion: ${env.BUILD_ID} en ${env.JENKINS_URL}"
            echo "Iniciando limpieza"
            sh 'rsync -a /home/gradle/data/ /home/gradle/.gradle'
            sh 'gradle --console=plain clean'
          }
        }
      }

      stage('Install dependencies - build') {
        steps {
          echo "Iniciando construccion"
          container('gradle') {
            script {
              sh "gradle --console=plain build copyLombokToLibOut -Dmaven.repo.login=${NEXUS_CREDENTIAL_USR} -Dmaven.repo.password=${NEXUS_CREDENTIAL_PSW} -x test -x check -x pmdMain --refresh-dependencies"
              sh 'rsync -a /home/gradle/.gradle/caches/ /home/gradle/data/caches'
            }
          }
        }
      }

      stage('Unit Test') {
        steps {
          container('gradle') {
            echo 'Test project'
            sh "gradle test --console=plain -Dmaven.repo.login=${NEXUS_CREDENTIAL_USR} -Dmaven.repo.password=${NEXUS_CREDENTIAL_PSW} -xcheck -x pmdMain --stacktrace "
          }
        }
        post {
          always {
            junit allowEmptyResults: true,
            testResults: '**/test-results.xml'
          }
        }
      }

      stage('Integration Test') {
        steps {
          container('gradle') {
            echo 'Test project'
            sh '''
                  gradle --console=plain integrationTest jacocoTestReport -Dmaven.repo.login=${NEXUS_CREDENTIAL_USR} -Dmaven.repo.password=${NEXUS_CREDENTIAL_PSW} --stacktrace
                  '''
          }
        }
        post {
          always {
            junit allowEmptyResults: true,
            testResults: '**/integration-test-results.xml'
          }
        }
      }

      stage('Jacoco Test Report') {
        steps {
          container('gradle') {
            echo 'Test report'
            sh "gradle --console=plain jacocoTestReport -Dmaven.repo.login=${NEXUS_CREDENTIAL_USR} -Dmaven.repo.password=${NEXUS_CREDENTIAL_PSW}"
          }
        }
      }

      stage('Sonarqube') {
        steps {
          sonarScanner()
        }
      }

      stage('Publish Artifact to Nexus') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          container('gradle') {
            echo "Publish Artifact to Nexus"
            sh "gradle upload -Dmaven.repo.login=${NEXUS_CREDENTIAL_USR} -Dmaven.repo.password=${NEXUS_CREDENTIAL_PSW}"
          }
        }
      }

      stage('Code Quality - Security') {
        steps {
          container('gradle') {
            script {
              try {
                sh 'gradle dependencyCheckAnalyze '
              } catch(err) {
                unstable('Code Quality - Security failed!')
              }
            }
          }
        }
      }

      stage('Code Quality - Helm Lint') {
        steps {
          helmLint()
        }
      }

      stage('Build docker') {
        steps {
          dockerBuild()
        }
      }

      stage('Publish docker image') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          dockerPush()
        }
      }

      stage('Package helm chart') {
        steps {
          helmPackage()
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          helmPush()
        }
      }

      /*stage('Delivery to qa' ) {
        when {
          expression { env.BRANCH_NAME == 'develop' }
        }
        steps{
          container('helm-kubectl') {
            echo 'Start delivery helm chart'
            withKubeConfig([credentialsId: 'k8s-cf-preproduccion', serverUrl:"https://rancherpre.favorita.ec/k8s/clusters/c-mw5ln" ]) {
              sh 'kubectl config current-context'
              sh 'kubectl config view'
              sh 'helm list --all-namespaces'
              sh "helm upgrade --install ${env.SHORT_NAME_APP} ./ci/helm --wait -n ${pipelineParams.namespace}" // --create-namespace  + in helm 3.2
            }
          }
        }  
      }

      stage('Deploy to prod' ) {
        when {
          expression { env.BRANCH_NAME == 'master' }
        }
        steps{
          container('helm-kubectl') {
            echo 'Start deploy helm chart'
            withKubeConfig([credentialsId: 'k8s-cf-preproduccion', serverUrl:"https://rancherpre.favorita.ec/k8s/clusters/c-mw5ln" ]) {
              sh 'kubectl config current-context'
              sh 'kubectl config view'
              sh 'helm list --all-namespaces'
              sh "helm upgrade --install ${env.SHORT_NAME_APP} ./ci/helm --wait -n ${pipelineParams.namespace} --force"  // --create-namespace  + in helm 3.2
            }
          }
        }  
      }*/

    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}
