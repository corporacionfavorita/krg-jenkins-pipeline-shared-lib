def call(Map config = [: ]) {
  container('sonar-scanner') {
    echo 'Code quality'
    withSonarQubeEnv('sonarqube') {
      script {
        sh "sonar-scanner -Dsonar.projectKey=${env.GIT_REPO_NAME} -Dsonar.projectName=${env.GIT_REPO_NAME} -Dsonar.projectVersion=${env.VERSION_APP} -Dsonar.branch.name=${env.BRANCH_NAME} -Dsonar.qualitygate.wait=true --debug"
      }
    }
  }
}