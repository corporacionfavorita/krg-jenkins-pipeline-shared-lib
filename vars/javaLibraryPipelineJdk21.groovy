def call(body) {
    def pipelineParams = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body()
    def projectName = env.JOB_NAME.split('/')[0]
    def labelGenerated = "java21-jnlp-slave-${projectName}"

    properties([durabilityHint('PERFORMANCE_OPTIMIZED'),])

    pipeline {
        agent {
            kubernetes {
                label labelGenerated
                yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: java21-jnlp-slave
      jenkins/java21-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk21
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: gradle
      image: gradle:8.7-jdk21
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: GRADLE_OPTS
        value: -Xms2g -Xmx4g -XX:MaxHeapSize=3g -Dfile.encoding=UTF-8 -XX:+UseG1GC -XX:+HeapDumpOnOutOfMemoryError
      tty: true
    imagePullSecrets:
    - name: regdock
    - name: cf-regcred      
  """
            }
        }

        environment {
            NXSJAVA_CREDENTIAL = credentials('cf-nexus')
        }

        triggers {
            bitbucketPush()
        }

        options {
            disableConcurrentBuilds()
            skipDefaultCheckout(true)
            buildDiscarder(logRotator(numToKeepStr: '10'))
            timeout(time: 30, unit: 'MINUTES')
        }

        stages {
            stage('Check Concurrent Builds') {
                steps {
                    checkCurrentBuilds()
                }
            }

            stage('Checkout code') {
                steps {
                    echo "Cambios en la rama ${env.BRANCH_NAME}\n"
                    echo 'Checkout code'
                    checkout scm
                }
            }

            stage('Set up environment') {
                steps {
                    container('git') {
                        script {
                            setupEnviromentGit()
                        }
                    }

                    container('gradle') {
                        script {
                            env.VERSION_APP = sh(script: "gradle -q printMainVersion -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} | tail -1", returnStdout: true).trim()
                            sh 'printenv'
                            gitFlowValidate()
                        }
                    }
                }
            }

            stage('Clean') {
                steps {
                    container('gradle') {
                        echo "Rama: ${env.BRANCH_NAME},  codigo de construccion: ${env.BUILD_ID} en ${env.JENKINS_URL}"
                        echo "Iniciando limpieza"
                        script {
                            sh "gradle --console=plain clean -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
                        }
                    }
                }
            }

            stage('Install dependencies - Build') {
                steps {
                    echo "Refresh dependencies"
                    container('gradle') {
                        script {
                            sh "gradle --refresh-dependencies --console=plain build copyLombokToLibOut -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} -x test -x check"
                        }
                    }
                }
            }

//      stage('Build') {
//        steps {
//          echo "Iniciando construccion"
//          container('gradle') {
//            script {
//                sh "gradle --console=plain build copyLombokToLibOut -Dmaven.repo.login=${NEXUS_CREDENTIAL_USR} -Dmaven.repo.password=${NEXUS_CREDENTIAL_PSW} -x test -x check -x pmdMain --quiet --build-cache --parallel"
//            }
//          }
//        }
//      }

            stage('Unit Test') {
                steps {
                    container('gradle') {
                        echo 'Test project'
                        sh "gradle test --console=plain -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} -x check -x pmdMain --stacktrace "
                    }
                }
                post {
                    always {
                        junit allowEmptyResults: true, testResults: '**/test-results.xml'
                    }
                }
            }

            stage('Jacoco Test Report') {
                steps {
                    container('gradle') {
                        echo 'Test project'
                        sh "gradle --console=plain jacocoTestReport -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
                    }
                }
            }

            stage('Code Quality - Sonarqube') {
                steps {
                    sonarScanner10()
                }
            }

            stage('Code Quality - Security') {
                steps {
                    container('gradle') {
                        script {
                            try {
                                sh "gradle dependencyCheckAnalyze -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
                            } catch (err) {
                                echo err
                                unstable('Code Quality - Security failed!')
                            }
                        }
                    }
                }
            }

            stage('Publish Artifact to Nexus') {
                when {
                    expression { env.PUBLISH.toBoolean() == true }
                }
                steps {
                    container('gradle') {
                        echo "Publish Artifact to Nexus"
                        sh "gradle publish -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
                    }
                }
            }
        }

        post {
            always {
                echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
            }
            success {
                slackNotifySuccess(pipelineParams.slackNotificationChannel)
            }
            failure {
                slackNotifyFailure(pipelineParams.slackNotificationChannel)
            }
            unstable {
                slackNotifyUnstable(pipelineParams.slackNotificationChannel)
            }
            changed {
                echo 'This will run only if the state of the Pipeline has changed'
                echo 'For example, if the Pipeline was previously failing but is now successful'
            }
        }
    }
}
