def call(body) {
    def pipelineParams = [: ]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body()
    def projectName = env.JOB_NAME.split('/')[0]
    def labelgenerated = "angular-slave-${projectName}"

    properties([
    durabilityHint('MAX_SURVIVABILITY')
  ])

    pipeline {
        agent {
            kubernetes {
                defaultContainer 'nodejs'
                label labelgenerated
                yaml '''
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: node14-jnlp-slave
      jenkins/angular-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: nodejs
      image: node:20
      imagePullPolicy: IfNotPresent
      securityContext:
      privileged: true
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: docker
      image: docker:19.03.8
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: cf-regcred
  '''
            }
        }

        environment {
            registryCredential = 'cf-nexus'
            NXSJS_CREDENTIAL = credentials('cf-nexus')
            NXSDOCKER_CREDENTIAL = credentials('cf-nexus')
            NXSHELM_CREDENTIAL = credentials('cf-nexus')
            NPM_USERNAME = 'jenkins'
            NPM_PASSWORD = 'amVua2luczo0UVBUZWtSdTJIYlVPWXY='
            NPM_EMAIL = 'jenkins@ec.krugercorp.com'
        }

        triggers {
            bitbucketPush()
        }

        options {
            disableConcurrentBuilds()
            skipDefaultCheckout(true)
            buildDiscarder(logRotator(numToKeepStr: '10'))
            timeout(time: 30, unit: 'MINUTES')
        }

        stages {
            stage('Check Concurrent Builds') {
                steps {
                    checkCurrentBuilds()
                }
            }

            stage('Checkout code') {
                steps {
                    sh 'printenv'
                    echo "Cambios en la rama ${env.BRANCH_NAME}\n"
                    echo 'Checkout code'
                    checkout scm
                }
            }

            stage('Set up environment') {
                steps {
                    setupEnviromentGit()

                    setupEnviromentAngularNode()

                    setupEnviromentDocker()

                    gitFlowValidate()
                }
            }

            stage('Install dependencies') {
                steps {
                    container('nodejs') {
                        echo 'Install NPM dependencies'
                        script {
                            try {
                                sh '''
                                npm cache clean --force
                                npm cache verify
                                npm install -f -d --quiet --loglevel=warn
                                '''
                            } catch (err) {
                                sh 'npm install -f --no-package-lock -d --quiet --loglevel=warn'
                            }
                        }
                    }
                }
            }

            stage('Code Quality - Security') {
                steps {
                    container('nodejs') {
                        script {
                            try {
                                sh 'npm i kng-utils@latest --save-dev'
                                sh 'node node_modules/kng-utils security'
                            } catch (err) {
                                unstable('Code Quality - Security failed!')
                            }
                        }
                    }
                }
            }

            stage('Build Packagr') {
                steps {
                    container('nodejs') {
                        echo 'Start build project'
                        sh 'npm run-script build:packagr'
                        sh '(ls -la dist/)'
                    }
                }
            }

            stage('Publish library') {
                steps {
                    container('nodejs') {
                        sh """
                            echo 'registry=https://cf1nxs.cfavorita.net/repository/npm-internal/' > ~/.npmrc
                            echo 'email=${NPM_EMAIL}' >> ~/.npmrc
                            echo 'always-auth=true' >> ~/.npmrc
                            echo '//cf1nxs.cfavorita.net/repository/npm-internal/:_auth=${NPM_PASSWORD}' >> ~/.npmrc
                        """
                     
                        echo 'Start publish project'
                        sh 'npm run-script publish:ci'
                    }
                }
            }

        }

        post {
            always {
                echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
            }
            success {
                slackNotifySuccess(pipelineParams.slackNotificationChannel)
            }
            failure {
                slackNotifyFailure(pipelineParams.slackNotificationChannel)
            }
            unstable {
                slackNotifyUnstable(pipelineParams.slackNotificationChannel)
            }
            changed {
                echo 'This will run only if the state of the Pipeline has changed'
                echo 'For example, if the Pipeline was previously failing but is now successful'
            }
        }
    }
}
