def call(){
    script{
        def nodeName = env.NODE_NAME
        echo "Running on node: ${nodeName}"
        def running = false;
        jenkins.model.Jenkins.get().computers.each { c ->
            if(c.node.labelString !='' && nodeName.contains(c.node.labelString)){
                if(running == true){
                    error "Otra construcción del proyecto está en ejecución"
                }else{
                    running = true
                }
            }
        }
    }
}