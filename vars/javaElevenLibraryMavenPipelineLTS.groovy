#!groovy

def call(body) {
    def pipelineParams = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body()

    def POM_HOME = pipelineParams.POM_HOME != null ? pipelineParams.POM_HOME : ''
    def VERSION = pipelineParams.version != null ? pipelineParams.version : 'latest'
      def projectName = env.JOB_NAME.split('/')[0]
    def labelgenerated = "java11-slave-${projectName}"

    pipeline {
        agent {
            kubernetes {
                label labelgenerated
                yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: java11-jnlp-slave
      jenkins/java11-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: maven
      image: cf1dck.cfavorita.net/krg-maven-jdk11:1.0.1-SNAPSHOT
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true     
    - name: docker
      image: docker:19.03.8
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock      
    imagePullSecrets:
    - name: regdock
    - name: cf-regcred    
  """
            }
        }

        environment {
            LC_ALL = 'en_US.UTF-8'
            LANG = 'en_US.UTF-8'
            LANGUAGE = 'en_US.UTF-8'
        }
        stages {
            stage('Check Concurrent Builds') {
                steps {
                checkCurrentBuilds()
                }
            }
            
            stage('Checkout code') {
                steps {
                    echo 'Checkout code'
                }
            }

            stage('Set up environment') {
                steps {
                    setupEnviromentGit()
                    container('maven') {
                        script {
                            dir(POM_HOME) {
                                //env.VERSION_APP = sh(script: "grep version pom.xml | grep -v -e '<?xml|~'| head -n 1 | sed 's/[[:space:]]//g' | sed -E 's/<.{0,1}version>//g' | awk '{print \$1}'", returnStdout: true).trim()
                                env.VERSION_APP = VERSION
                            }
                        }
                    }
                }
            }

            stage('Clean') {
                steps {
                    container('maven') {
                        script {
                            dir(POM_HOME) {
                                sh "mvn clean"
                            }
                        }
                    }
                }
            }

            stage('Install dependencies - Build') {
                steps {
                    container('maven') {
                        script {
                            dir(POM_HOME) {
                                sh "mvn install"
                            }
                        }
                    }
                }
            }
            stage('Unit test') {
                steps {
                    container('maven') {
                        script {
                            dir(POM_HOME) {
                                sh "mvn test"
                            }
                        }
                    }
                }
            }

            stage('Jacoco Test Report') {
                steps {
                    container('maven') {
                        script {
                            dir(POM_HOME) {
                                sh "mvn verify"
                            }
                        }
                    }
                }
            }

            stage('Code Quality - Sonarqube') {
                steps {
                    sonarScanner10()
                }
            }

            stage('Code Quality - Security') {
                steps {
                    container('maven') {
                        script {
                            try {
                                dir(POM_HOME) {
                                    sh "mvn verify -DfailBuildOnCVSS=7"
                                }
                            }catch (err) {
                                unstable('Code Quality - Security failed!')
                            }
                        }
                    }
                }
            }
            stage('Publish to Nexus Repository') {
                when {
                    expression {
                        env.BRANCH_NAME == 'develop' || env.BRANCH_NAME == 'master' || env.BRANCH_PREFIX == 'hotfix'
                    }
                }
                steps {
                    container('maven') {
                        script {
                            dir(POM_HOME) {
                                sh 'mvn clean install deploy'
                            }
                        }
                    }
                }
            }
        }

        post {
            always {
                echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
            }
            success {
                slackNotifySuccess(pipelineParams.slackNotificationChannel)
            }
            failure {
                slackNotifyFailure(pipelineParams.slackNotificationChannel)
            }
            unstable {
                slackNotifyUnstable(pipelineParams.slackNotificationChannel)
            }
            aborted {
                slackNotifyAborted(pipelineParams.slackNotificationChannel)
            }
            changed {
                echo 'This will run only if the state of the Pipeline has changed'
                echo 'For example, if the Pipeline was previously failing but is now successful'
            }
        }
    }

}





