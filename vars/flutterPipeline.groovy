
def call(body) {
  def pipelineParams = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()

   properties([
          //buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '', numToKeepStr: '10')),
          durabilityHint('MAX_SURVIVABILITY'),
         //parameters([string(defaultValue: '', description: '', name: 'run_stages')])
  ])


  pipeline {
    //containerTemplate(name: 'jnlp', image: 'jenkins/jnlp-slave:3.35-5-alpine', args: '${computer.jnlpmac} ${computer.name}'),
    agent {
      kubernetes {
        defaultContainer 'nodejs'
        label 'flutter-slave'
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: flutter-jnlp-slave
      jenkins/flutter-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: flutter
      image: cf1dckspt.cfavorita.net/flutter/kruger-flutter3-jdk11:1.0.12
      imagePullPolicy: Always
      securityContext:
      privileged: true
      command:
      - cat
      tty: true
    - name: nodejs
      image: cf1dck.cfavorita.net/ec.com.smx.jenkins/node14-jnlp-slave:1.0.0-RELEASE
      imagePullPolicy: IfNotPresent
      securityContext:
      privileged: true
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true  
    - name: docker
      image: docker:19.03.8
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: cf-regcred
    - name: cf-reg-cred
  """
      }
    }

    // environment {
    //   registryCredential = 'nexuskruger'
    //   DOCKER_CREDENTIAL = credentials('nexuskruger')
    // }

    // triggers {
    //   bitbucketPush() 
    // }
    
    // options {
    //   disableConcurrentBuilds()
    //   //skipStagesAfterUnstable()
    //   skipDefaultCheckout(true)
    //   buildDiscarder(logRotator(numToKeepStr: '10'))
    //   timeout(time: 30, unit: 'MINUTES')
    // }

    
    environment {
      registryCredential = 'cf-nexus'
      NXSJAVA_CREDENTIAL = credentials('cf-nexus')
      NXSDOCKER_CREDENTIAL = credentials('cf-nexus')
      NXSHELM_CREDENTIAL = credentials('cf-nexus')
      envDev = 'ENVIRONMENT=DEV'
      envProd = 'ENVIRONMENT=PROD'
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 30, unit: 'MINUTES')
    }

    stages {
      stage('Checkout code') {      
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
         
          
        }
      }

     
      stage('Set up environment') {
        steps {
          setupEnviromentGit()

          setupEnviromentAngularNode()
          
          setupEnviromentDocker()
        }
      }

      stage('Install dependencies') {
        steps {
          container('flutter') {
             sh '''
                flutter --version
            '''
            sh '''
               flutter clean
            '''
            echo 'Install dependencies'
            sh '''
                flutter pub get
            '''
          }
        }
      }

       stage('Test') {      
        steps {
          container('flutter') {
          echo "Realizando Flutter Test"
           sh '''
                flutter test
            '''
          }
        }
      }

      stage('Sonarqube') {
        steps {
          sonarScanner()
        }
      }
      

      stage('Build') {
        
        steps {
          container('flutter') { 
            script{
                    if(env.BRANCH_NAME == 'master' || env.BRANCH_PREFIX == 'hotfix'){
              echo 'Start build project'
             sh '''
                flutter build apk --dart-define=${envProd}
            '''
            } else if (env.BRANCH_NAME == 'develop' || env.BRANCH_PREFIX == 'feature'){
               echo 'Start build project'
             sh '''
                flutter build apk --dart-define=${envDev}
            '''
            }
            }
          }
        }
      }
    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }        
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      changed {
          echo 'This will run only if the state of the Pipeline has changed'
          echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }  
}
