def call(String slackNotificationChannel = 'devops') {
  echo "La linea de construccion finalizo con errores ${currentBuild.fullDisplayName} with ${env.BUILD_URL}"
  slackSend (
    iconEmoji: ":robot_face",
    channel: slackNotificationChannel, 
    color: "danger", 
    message: ":disappointed_relieved::sos: *ERROR EN CONSTRUCCIÓN* en construcción ${currentBuild.fullDisplayName}\n *Repositorio:* `${env.GIT_REPO_NAME}`\n *Rama:* `${env.BRANCH_NAME}`\n *Autor:* ${env.GIT_USER_NAME} [${env.GIT_USER_EMAIL}]\n *Commit:* ${env.GIT_COMMIT}\n *Construcción:* ${env.BUILD_NUMBER}| <${env.BUILD_URL}|Revisar>"
  )
}
