def call(Map config = [: ]) {
  container('helm-kubectl') {
    script {
      echo 'Start publish helm chart'
      sh 'helm plugin install --version master https://corenegocio@bitbucket.org/corporacionfavorita/helm-nexus-push.git'
      if (env.HAS_SUBPROJECTS == "false") {
        retry(3) {          
          sh "helm nexus-push . ./ci/helm  -u ${env.NXSHELM_CREDENTIAL_USR} -p ${env.NXSHELM_CREDENTIAL_PSW} -av ${env.VERSION_APP}"
        }
      } else {
        def projectDirsList = env.PUBLISH_APP_PATHS.split('\n')
        for (projectDir in projectDirsList) {
          retry(3) {
            sh "helm nexus-push . ./${projectDir}/ci/helm  -u ${env.NXSHELM_CREDENTIAL_USR} -p ${env.NXSHELM_CREDENTIAL_PSW} -av ${env.VERSION_APP}"
          }
        }
      }
    }
  }
}