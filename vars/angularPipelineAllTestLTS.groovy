def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()
  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "angular-slave-${projectName}"

  properties([
    durabilityHint('MAX_SURVIVABILITY')
  ])

  pipeline {
    agent {
      kubernetes {
        defaultContainer 'nodejs'
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: node14-jnlp-slave
      jenkins/angular-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: nodejs
      image: cf1dck.cfavorita.net/ec.com.smx.jenkins/node14-jnlp-slave:1.0.1-RELEASE
      imagePullPolicy: IfNotPresent
      securityContext:
      privileged: true
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true  
    - name: docker
      image: docker:19.03.8
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: cf-regcred  
  """
      }
    }

    environment {
      registryCredential = 'cf-nexus'
      NXSJAVA_CREDENTIAL = credentials('cf-nexus')
      NXSDOCKER_CREDENTIAL = credentials('cf-nexus')
      NXSHELM_CREDENTIAL = credentials('cf-nexus')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 20, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
        steps {
          checkCurrentBuilds()
        }
      }
      
      stage('Checkout code') {
        steps {
          sh 'printenv'
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          setupEnviromentGit()

          setupEnviromentAngularNode()
          
          setupEnviromentDocker()
          
          gitFlowValidate()
        }
      }

      stage('Install dependencies') {
        steps {
          container('nodejs') {
            echo 'Install NPM dependencies'
            script {
              try {
                sh '''
                  npm cache clean --force
                  npm cache verify
                  npm install -f -d --quiet --loglevel=warn
                '''
              } catch(err) {
                sh 'npm install -f --no-package-lock -d --quiet --loglevel=warn'
              }
            }
          }
        }
      }
      stage('Unit Test') {
        steps {
          container('nodejs') {
            echo 'Test project'
            script {
              try {
                sh '''
                  npm run-script test:unit
                '''
              } catch(err) {
                unstable('Test unit - failed!')
              }
            }
          }
        }
        post {
          always {
            junit allowEmptyResults: true, testResults: '**/test-results.xml'
          }
        }
      }
      
      stage('Integration Test') {
        steps {
          container('nodejs') {
            echo 'Test project'
            script {
              try {
                sh '''
                  npm run-script test:integration
                '''
              } catch(err) {
                unstable('Integration Test failed!')
              }
            }
          }
        }
      }  

      stage('Code Quality - Lint') {
        steps {
          container('nodejs') {
            echo 'Code quality'
            script {
              sh 'npm run-script lint:ci'
            }
          }
        }
        post {
          always {
            recordIssues enabledForFailure: true, aggregatingResults: true, tool: checkStyle(pattern: 'checkstyle-result.xml')
          }
        }
      }

      stage('Code Quality - Sonarqube') {
        steps {
          container('nodejs') {
            echo 'Code quality'
            script {                  
              try {
                sh "npm run-script cover"
              } catch(err) {
                unstable('Unit testing - failed!')
              }
            }
          }
          sonarScanner10()
        }
      }

      stage('Code Quality - Security') {
        steps {
          container('nodejs') {
            script {
              try {
                sh 'npm i kng-utils@latest --save-dev'
                sh 'node node_modules/kng-utils security'
              } catch (err) {
                unstable('Code Quality - Security failed!')
              }
            }
          }
        }
      }

      stage('Code Quality - helm lint') {
        steps {
          helmLint()
        }
      }

      stage('Build') {
        steps {
          container('nodejs') {
            echo 'Start build project'
            sh 'npm run-script build:ci'
          }
        }
      }

      stage('Build docker') {
        steps {
          dockerBuild()
        }
      }

      stage('Publish docker image') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          dockerPush()
        }
      }

      stage('Package helm chart') {
        steps {
          helmPackage()
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          helmPush()
        }
      }

      stage('Delivery to qa') {
        when {
          expression {
            (env.PUBLISH.toBoolean() == true) && pipelineParams.enableContinuosDelivery == true
          }
        }
        options {
          timeout(time: 300, unit: 'SECONDS') 
        }                
        steps {
          helmDeploy(slackNotificationChannel: pipelineParams.slackNotificationChannel, namespace: pipelineParams.namespace)
        }
      }

/*       stage('Deploy to prod' ) {
        when {
          expression { env.BRANCH_NAME == 'develop' }
        }
        steps{
          container('helm-kubectl') {
            echo 'Start deploy helm chart'
            withKubeConfig([credentialsId: 'k8s-cf-preproduccion', serverUrl:"https://rancherpre.favorita.ec/k8s/clusters/c-mw5ln"]) {
              sh 'kubectl config current-context'
              sh "helm upgrade --install ${env.SHORT_NAME_APP} ./ci/helm --wait -n ${pipelineParams.namespace} --force"  // --create-namespace  + in helm 3.2
            }
          }
        }  
      } */

      /*stage('Katalon test'){
        when {
          expression { env.BRANCH_NAME == 'develop' || env.BRANCH_NAME == 'master' }
        }
        steps {
          container('katalon') {
            script {
              sh "pwd"
              if (fileExists('end-to-end')) {
                sh "pwd"
                echo "test version exec: ${env.VERSION_TAG}"
                echo 'Start katalon'
                sh 'katalonc.sh -projectPath="./end-to-end" -browserType="Firefox" -retry=0 -statusDelay=15 -testSuitePath="Test Suites/test" -apiKey="ae74a191-2cb0-4cf0-a61e-1b1d4ffd5774" -sendMail=caguilar@ec.krugercorp.com'
              } else {
                echo 'No exists directory'
              }
            }
          }
        }
      }*/
    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      aborted {
        slackNotifyAborted(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}
