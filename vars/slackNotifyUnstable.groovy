def call(String slackNotificationChannel = 'devops') {
  echo 'La linea de construccion finalizo de forma inestable' 
  slackSend (
    iconEmoji: ":robot_face",
    channel: slackNotificationChannel, 
    color: "warning", 
    message: ":worried::warning: *ALERTA CONSTRUCCIÓN INESTABLE* en construcción ${currentBuild.fullDisplayName}\n *Repositorio:* `${env.GIT_REPO_NAME}`\n *Rama:* `${env.BRANCH_NAME}`\n *Autor:* ${env.GIT_USER_NAME} [${env.GIT_USER_EMAIL}]\n *Commit:* ${env.GIT_COMMIT}\n *Construcción:* ${env.BUILD_NUMBER}| <${env.BUILD_URL}|Revisar>"
  )
}
