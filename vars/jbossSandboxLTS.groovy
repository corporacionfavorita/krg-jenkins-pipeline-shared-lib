def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()
  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "java8-slave-${projectName}"

  properties([
    durabilityHint('PERFORMANCE_OPTIMIZED')
  ])

  pipeline {
    agent {
      kubernetes {
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: java8-jnlp-slave
      jenkins/java8-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080 
      tty: true
    - name: gradle
      image: cf1dck.cfavorita.net/jdk/gradle:4.10-jdk-alpine
      imagePullPolicy: IfNotPresent
      command:
      - cat
      volumeMounts:
      - mountPath: "/home/gradle/data"
        name: "gradle-pv-storage"      
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080 
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080
      - name: GRADLE_OPTS
        value: -Xms2g -Xmx4g -XX:MaxHeapSize=3g -Dfile.encoding=UTF-8 -XX:+UseG1GC -XX:+HeapDumpOnOutOfMemoryError
      tty: true     
    - name: docker
      image: docker:19.03.8
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
      env:
      - name: HTTP_PROXY
        value: http://pxy12:8080
      - name: HTTPS_PROXY
        value: http://pxy12:8080        
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
      - name: gradle-pv-storage
        persistentVolumeClaim:
          claimName: "gradle-pv-nfs-claim-jboss"
          readOnly: false          
    imagePullSecrets:
    - name: regdock
    - name: cf-reg-cred    
  """
      }
    }

    environment {
      registryCredential = 'cf-nexus'
      DOCKER_CREDENTIAL = credentials('cf-nexus')
      NEXUS_CREDENTIAL = credentials('cf-nexus')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 180, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
        steps {
          checkCurrentBuilds()
        }
      }
      
      stage('Checkout code') {
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          container('git') {
            script {
              setupEnviromentGit()
            }
            gitFlowValidate()
          }

          container('gradle') {
            script {
              env.VERSION_APP = sh(script: "gradle -q printMainVersion | tail -1", returnStdout: true).trim()
              env.NAME_APP = sh(script: "gradle -q printProjectName | tail -1", returnStdout: true).trim()
              env.NAME_APP_LIST = sh(script: "gradle -q printProjectName", returnStdout: true).trim()
              env.PUBLISH_APP_PATHS = sh(script: "gradle -q printDockerProjectNames", returnStdout: true).trim()
              env.HAS_SUBPROJECTS = sh(script: "gradle -q hasDockerSubModules| tail -1", returnStdout: true).trim()
              env.SHORT_NAME_APP = env.NAME_APP.substring(env.NAME_APP.lastIndexOf('/') + 1, env.NAME_APP.length())
              env.DOCKER_REGISTRY = sh(script: "gradle -q printRegistry | tail -1", returnStdout: true).trim()
              env.DOCKER_REGISTRY_IMAGE = env.DOCKER_REGISTRY + '/' + env.NAME_APP.replace('@', '') + ':' + env.VERSION_APP
              env.DOCKER_REGISTRY_IMAGE_LATEST = env.DOCKER_REGISTRY + '/' + env.NAME_APP.replace('@', '') + ':latest'
              sh 'printenv'
            }
          }
        }
      }

      stage('Clean') {
        steps {
          container('gradle') {
            echo "Rama: ${env.BRANCH_NAME},  codigo de construccion: ${env.BUILD_ID} en ${env.JENKINS_URL}"
            echo "Iniciando limpieza"
            sh 'rsync -a /home/gradle/data/ /home/gradle/.gradle'
            sh 'gradle --console=plain clean'
          }
        }
      }

      stage('Build') {
        steps {
          echo "Iniciando construccion"
          container('gradle') {
            script {
              sh "gradle build copyLombokToLibOut -Dmaven.repo.login=${NEXUS_CREDENTIAL_USR} -Dmaven.repo.password=${NEXUS_CREDENTIAL_PSW} -x test -x pmdMain -x check --quiet --build-cache --parallel"
              sh 'rsync -a /home/gradle/.gradle/caches/ /home/gradle/data/caches'
            }
          }
        }
      }

      stage('Test/QC') {
        failFast true
        parallel {
          stage('Code Quality - Sonarqube') {
            stages {
              stage('Unit Test') {
                when {
                  expression {
                    pipelineParams.omitTest == null || pipelineParams.omitTest == false
                  }
                }
                steps {
                  container('gradle') {
                    script {
                      echo 'Test project'
                      try {
                        sh "gradle test --console=plain -Dmaven.repo.login=${NEXUS_CREDENTIAL_USR} -Dmaven.repo.password=${NEXUS_CREDENTIAL_PSW} -xcheck -x pmdMain --stacktrace "
                      } catch(err) {
                        unstable('Unit Test - Unit Test failed!')
                      }
                    }
                  }
                }
                post {
                  always {
                    junit allowEmptyResults: true,
                    testResults: '**/test-results.xml'
                  }
                }
              }

              stage('Jacoco Test Report') {
                steps {
                  container('gradle') {
                    echo 'Test report'
                    sh "gradle --console=plain jacocoTestReport -Dmaven.repo.login=${NEXUS_CREDENTIAL_USR} -Dmaven.repo.password=${NEXUS_CREDENTIAL_PSW}"
                  }
                }
              }

              stage('Sonarqube') {
                steps {
                  sonarScanner10()
                }
              }
            }
          }

          stage('Code Quality - helm lint') {
            steps {
              helmLint()
            }
          }
        }
      }

      stage('Build docker') {
        steps {
          dockerBuild()
        }
      }

      stage('Publish docker image') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          dockerPush()
        }
      }

      stage('Package helm chart') {
        steps {
          helmPackage()
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          helmPush()
        }
      }
    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}