import ec.com.smx.Constants
import ec.com.smx.Messages
def call() {
    script {
        //echo " Error stage: ${env.CURRENT_STAGE}"
        // echo "Current Result: ${currentBuild.currentResult}"
        // echo "Result: ${currentBuild.result}"
        switch (env.CURRENT_STAGE) {
                case Constants.STAGES.CHECK_CURRENT_BUILDS.name():
                    iterateSuggestion(Messages.CHECK_CURRENT_BUILDS)
                    break
                case Constants.STAGES.CHECKOUT_CODE.name():
                    iterateSuggestion(Messages.CHECKOUT_CODE)
                    break
                case Constants.STAGES.SETUP_ENVIRONMENT.name():
                    iterateSuggestion(Messages.SETUP_ENVIRONMENT)
                    break
                case Constants.STAGES.CLEAN.name():
                    iterateSuggestion(Messages.CLEAN)
                    break
                case Constants.STAGES.BUILD.name():
                    iterateSuggestion(Messages.BUILD)
                    break
                case Constants.STAGES.UNIT_TEST.name():
                    iterateSuggestion(Messages.UNIT_TEST)
                    break
                case Constants.STAGES.INTEGRATION_TEST.name():
                    iterateSuggestion(Messages.INTEGRATION_TEST)
                    break
                case Constants.STAGES.JACOCO_TEST.name():
                    iterateSuggestion(Messages.JACOCO_TEST)
                    break
                case Constants.STAGES.SONARQUBE.name():
                    iterateSuggestion(Messages.SONARQUBE)
                    break
                case Constants.STAGES.PUBLISH_DEPENDENCY.name():
                    iterateSuggestion(Messages.PUBLISH_DEPENDENCY)
                    break
                case Constants.STAGES.SECURITY.name():
                    iterateSuggestion(Messages.SECURITY)
                    break
                case Constants.STAGES.HELM_LINT.name():
                    iterateSuggestion(Messages.HELM_LINT)
                    break
                case Constants.STAGES.DOCKER_BUILD.name():
                    iterateSuggestion(Messages.DOCKER_BUILD)
                    break
                case Constants.STAGES.PUBLISH_DOCKER.name():
                    iterateSuggestion(Messages.PUBLISH_DOCKER)
                    break
                case Constants.STAGES.PACKAGE_HELM.name():
                    iterateSuggestion(Messages.PACKAGE_HELM)
                    break
                case Constants.STAGES.PUBLISH_HELM.name():
                    iterateSuggestion(Messages.PUBLISH_HELM)
                    break
                case Constants.STAGES.INSTALL_ANG.name():
                    iterateSuggestion(Messages.INSTALL_ANG)
                    break
                case Constants.STAGES.SETUP_ENVIRONMENT_ANG.name():
                    iterateSuggestion(Messages.SETUP_ENVIRONMENT_ANG)
                    break
                case Constants.STAGES.UNIT_TEST_ANG.name():
                    iterateSuggestion(Messages.UNIT_TEST_ANG)
                    break
                case Constants.STAGES.INTEGRATION_TEST_ANG.name():
                    iterateSuggestion(Messages.INTEGRATION_TEST_ANG)
                    break
                case Constants.STAGES.QUALITY_LINT_ANG.name():
                    iterateSuggestion(Messages.QUALITY_LINT_ANG)
                    break
                case Constants.STAGES.BUILD_ANG.name():
                    iterateSuggestion(Messages.BUILD_ANG)
                    break
                case Constants.STAGES.SECURITY_ANG.name():
                    iterateSuggestion(Messages.SECURITY_ANG)
                    break 
                default:
                    echo "Opción no reconocida"
                    break
        }
    }
}

def iterateSuggestion(def messages) {
    messages.eachWithIndex{val, index ->
        index++
        println Constants.FAIL_SUGGESTION + index + ': ' + val                  
    } 
}