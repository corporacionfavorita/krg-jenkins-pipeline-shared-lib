def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()

  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "angular-slave-${projectName}"

  properties([
  durabilityHint('MAX_SURVIVABILITY')])

  pipeline {
    agent {
      kubernetes {
        defaultContainer 'nodejs'
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: angular8-jnlp-slave
      jenkins/angular-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: nodejs
      image: docker.krugernetes.com/ec.com.kruger.jenkinsci/angular10-jnlp-slave:1.0.0
      imagePullPolicy: IfNotPresent
      securityContext:
      privileged: true
      command:
      - cat
      env:
      - name: http_proxy
        value: pxy12:8080
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080 
      tty: true  
    - name: docker
      image: docker:19.03.8
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: pxy12:8080
      - name: https_proxy
        value: pxy12:8080
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: cf-regcred  
  """
      }
    }

    environment {
      registryCredential = 'cf-nexus'
      DOCKER_CREDENTIAL = credentials('cf-nexus')
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 30, unit: 'MINUTES')
    }

    stages {
      stage('Checkout code') {
        steps {
          sh 'printenv'
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          setupEnviromentGit()
          container('git') {
            script {
              env.HAS_SUBPROJECTS = "false"
            }
          }
          setupEnviromentAngularNode()
        }
      }

      stage('Trigger') {
        steps {
          echo 'Init Run cnc-autorizaciones-usuario-ng-cd pipeline!'
          build job: "cnc-autorizaciones-usuario-ng-cd/${env.JOB_BASE_NAME}", propagate: false, wait: false
          echo 'Run cnc-autorizaciones-usuario-ng-cd pipeline!'
        }
      }   
    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {        
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      aborted {
		    slackNotifyAborted(pipelineParams.slackNotificationChannel)
        echo 'Why didn\'t you aprove the deploy?'        
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}