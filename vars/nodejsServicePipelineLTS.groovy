def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()
  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "node-slave-${projectName}"

  properties([
    durabilityHint('PERFORMANCE_OPTIMIZED')
  ])

  pipeline {
    agent {
      kubernetes {
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: angular8-jnlp-slave
      jenkins/angular-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      resources:
        requests:
          memory: "32Mi"
          cpu: "10m"
        limits:
          memory: "4000Mi"
          cpu: "100m"
      command:
      - cat
      tty: true
    - name: nodejs
      image: cf1dck.cfavorita.net/ec.com.smx.jenkins/node14-jnlp-slave:1.0.0-RELEASE
      imagePullPolicy: IfNotPresent
      resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "4000Mi"
          cpu: "1000m"
      securityContext:
      privileged: true
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true  
    - name: docker
      image: docker:latest
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: cf-regcred  
  """
      }
    }

    environment {
      registryCredential = 'cf-nexus'
      NXSDOCKER_CREDENTIAL = credentials('cf-nexus')
      NXSHELM_CREDENTIAL = credentials('cf-nexus')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 15, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
        steps {
          checkCurrentBuilds()
        }
      }
      
      stage('Checkout code') {
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          setupEnviromentGit()
          
          setupEnviromentAngularNode()

          setupEnviromentDocker()
                    
          gitFlowValidate()
        }
      }

      stage('Install dependencies') {
        steps {
          container('nodejs') {
            echo 'Install NPM dependencies'
            script {
              try {
                sh '''
                  npm cache clean --force
                  npm cache verify
                  npm install -f -d --quiet --loglevel=warn
                '''
              } catch(err) {
                sh 'npm install -f --no-package-lock -d --quiet --loglevel=warn'

              }
            }
          }
        }
      }

      stage('Test/QC') {
        parallel {
          stage('Unit Test') {
            steps {
              container('nodejs') {
                echo 'Test project'
                sh '''
                  npm run-script test:unit
                '''
              }
            }
            post {
              always {
                junit allowEmptyResults: true, testResults: '**/test-results.xml'
              }
            }
          }

          stage('Code Quality - Lint') {
            steps {
              container('nodejs') {
                echo 'Code quality'
                sh 'npm run-script lint:ci'
              }
            }
            post {
              always {
                recordIssues enabledForFailure: true, aggregatingResults: true, tool: checkStyle(pattern: 'checkstyle-result.xml')
              }
            }
          }

          stage('Code Quality - Sonarqube') {
            steps {
              container('nodejs') {
                echo 'Code quality'
                sh "npm run-script cover"
              }
              container('sonar-scanner') {
                echo 'Code quality'
                sonarScanner10()
              }
            }
          }

          stage('Code Quality - Security') {
            steps {
              container('nodejs') {
                script {
                  try {
                    sh "npm run-script test:security"
                  } catch(err) {
                    unstable('Code Quality - Security failed!')
                  }
                }
              }
            }
          }

          stage('Code Quality - helm lint') {
            steps {
              helmLint()
            }
          }
        }
      }

      stage('Build') {
        steps {
          container('nodejs') {
            echo 'Start build project'
            sh 'npm run-script build:ci'
          }
        }
      }

      stage('Integration Test') {
        steps {
          container('nodejs') {
            echo 'Test project'
            script {
              try {
                sh '''
                  npm run-script test:integration
                '''
              } catch(err) {
                unstable('Integration Test failed!')
              }
            }
          }
        }
      }

      stage('Build docker') {
        steps {
          dockerBuild()
        }
      }

      stage('Publish docker image') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          dockerPush()
        }
      }

      stage('Package helm chart') {
        steps {
          helmPackage()
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          helmPush()
        }
      }
    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}