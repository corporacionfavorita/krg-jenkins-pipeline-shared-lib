def call(Map config = [: ]) {
  container('helm-kubectl') {
    script {
      echo 'Start build helm chart'
      sh "helm repo add cf-helm https://cf1hlmnxs.cfavorita.net/repository/helm/ --username ${env.NXSHELM_CREDENTIAL_USR} --password ${env.NXSHELM_CREDENTIAL_PSW}"
      sh "helm repo add cf-helm-base https://cf1hlmnxs.cfavorita.net/repository/helm-base/ --username ${env.NXSHELM_CREDENTIAL_USR} --password ${env.NXSHELM_CREDENTIAL_PSW}"

      if (env.HAS_SUBPROJECTS == "false") {
        retry(3) {
          sh 'helm dep update ./ci/helm'
        }
        sh 'helm package ./ci/helm'
      } else {
        def projectDirsList = env.PUBLISH_APP_PATHS.split('\n')
        for (projectDir in projectDirsList) {
          echo "Build helm chart sub ${projectDir}"
          retry(3) {
            sh "helm dep update ./${projectDir}/ci/helm"
          }
          sh "helm package ./${projectDir}/ci/helm"
        }
      }
    }
  }
}