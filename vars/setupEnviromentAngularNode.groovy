def call(Map config = [: ]) {
  container('nodejs') {
    script {
      env.VERSION_APP = sh(script: "npm run-script version |tail -1", returnStdout: true).trim()
      env.NAME_APP = sh(script: "npm run-script name |tail -1", returnStdout: true).trim()
      env.SHORT_NAME_APP = env.NAME_APP.substring(env.NAME_APP.lastIndexOf('/') + 1, env.NAME_APP.length())
      env.HAS_SUBPROJECTS = "false"
      sh 'printenv'
    }
  }
}