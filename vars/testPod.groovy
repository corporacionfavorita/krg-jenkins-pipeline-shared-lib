def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()
  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "angular-js-slave-${projectName}"

  properties([
    durabilityHint('PERFORMANCE_OPTIMIZED')
  ])

  pipeline {
    agent {
      kubernetes {
        defaultContainer 'nodejs'
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: angular-js-jnlp-slave
      jenkins/angular-js-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      resources:
        requests:
          memory: "32Mi"
          cpu: "10m"
        limits:
          memory: "4000Mi"
          cpu: "100m"
      command:
      - cat
      tty: true
    - name: nodejs
      image: docker.krugernetes.com/ec.com.kruger.jenkinsci/angular-js-jnlp-slave:1.2.0-rc 
      imagePullPolicy: Always
      resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "4000Mi"
          cpu: "1000m"
      securityContext:
      privileged: true
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: http://pxy12:8080
      - name: https_proxy
        value: http://pxy12:8080 
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: http://pxy12:8080
      - name: https_proxy
        value: http://pxy12:8080 
      tty: true  
    - name: docker
      image: docker:19.03.8
      imagePullPolicy: IfNotPresent
      resources:
        requests:
          memory: "64Mi"
          cpu: "100m"
        limits:
          memory: "4000Mi"
          cpu: "200m"
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      env:
      - name: JAVA_OPTS
        value: -Dhttp.proxyHost=pxy12 -Dhttp.proxyPort=8080 -Dhttps.proxyHost=pxy12 -Dhttps.proxyPort=8080
      - name: http_proxy
        value: http://pxy12:8080
      - name: https_proxy
        value: http://pxy12:8080
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: cf-regcred  
  """
      }
    }

    environment {
      registryCredential = 'cf-nexus'
      DOCKER_CREDENTIAL = credentials('cf-nexus')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 30, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
        steps {
          checkCurrentBuilds()
        }
      }
      
      stage('Checkout code') {
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          setupEnviromentGit()         
          gitFlowValidate()          
          container('git') {
            script {
              env.HAS_SUBPROJECTS = "false"
            }
          }
          //setupEnviromentAngularNode()
        }
      }

      stage('Package helm chart') {
        steps {
          helmPackage()
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          helmPush()
        }
      }

      stage('Delivery to qa') {
        when {
          expression {
            (env.PUBLISH.toBoolean() == true) && pipelineParams.enableContinuosDelivery == true
          }
        }
        options {
          timeout(time: 300, unit: 'SECONDS') 
        }                
        steps {
          helmDeploy(slackNotificationChannel: pipelineParams.slackNotificationChannel, namespace: pipelineParams.namespace)
        }
      }

    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      aborted {
		    slackNotifyAborted(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}