def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()
  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "angular-js-slave-${projectName}"

  properties([
    durabilityHint('PERFORMANCE_OPTIMIZED')
  ])

  pipeline {
    agent {
      kubernetes {
        defaultContainer 'nodejs'
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: angular-js-jnlp-slave
      jenkins/angular-js-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      resources:
        requests:
          memory: "32Mi"
          cpu: "10m"
        limits:
          memory: "4000Mi"
          cpu: "100m"
      command:
      - cat
      tty: true
    - name: nodejs
      image: cf1dck.cfavorita.net/ec.com.smx.jenkins/angularjs-agent:1.0.0-RELEASE
      imagePullPolicy: Always
      resources:
        requests:
          memory: "256Mi"
          cpu: "250m"
        limits:
          memory: "4000Mi"
          cpu: "1000m"
      securityContext:
      privileged: true
      command:
      - cat 
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true  
    - name: docker
      image: docker:19.03.8
      imagePullPolicy: IfNotPresent
      resources:
        requests:
          memory: "64Mi"
          cpu: "100m"
        limits:
          memory: "4000Mi"
          cpu: "200m"
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: cf-regcred  
  """
      }
    }

    environment {
      registryCredential = 'cf-nexus'
      NXSJAVA_CREDENTIAL = credentials('cf-nexus')
      NXSDOCKER_CREDENTIAL = credentials('cf-nexus')
      NXSHELM_CREDENTIAL = credentials('cf-nexus')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 15, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
        steps {
          checkCurrentBuilds()
        }
      }
      
      stage('Checkout code') {
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          setupEnviromentGit()

          setupEnviromentAngularNode()

          setupEnviromentDocker()

          gitFlowValidate()
        }
      }

      stage('Install dependencies') {
        steps {
          container('nodejs') {
            echo 'test'
            echo 'Install JSPM dependencies'
            sh 'cat ~/.jspm/config'  
            //sh '''
            //    jspm config registries.github.remote https://cf1nxs.cfavorita.net/repository/npm-all/
            //'''

            echo 'Install NPM dependencies' 
            sh '''
                npm install -f --no-package-lock --prefer-offline --no-audit --quiet
            '''

            echo 'PostInstall NPM dependencies'
            sh '''
                npm run-script postinstall -f --no-package-lock --quiet 
            '''
          }
        }
      }

      stage('Test/QC') {
        parallel {
          stage('Code Quality - Sonarqube') {
            steps {
              sonarScanner()
            }
          }

          stage('Code Quality - helm lint') {
            steps {
              helmLint()
            }
          }
        }
      }

      stage('Build') {
        steps {
          container('nodejs') {
            echo 'Start build project'
            sh 'npm run-script build:ci'
          }
        }
      }

      stage('Build docker') {
        steps {
          dockerBuild()
        }
      }

      stage('Publish docker image') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          dockerPush()
        }
      }

      stage('Package helm chart') {
        steps {
          helmPackage()
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          helmPush()
        }
      }

      stage('Delivery to qa') {
        when {
          expression {
            (env.PUBLISH.toBoolean() == true) && pipelineParams.enableContinuosDelivery == true
          }
        }
        options {
          timeout(time: 300, unit: 'SECONDS') 
        }                
        steps {
          helmDeploy(slackNotificationChannel: pipelineParams.slackNotificationChannel, namespace: pipelineParams.namespace)
        }
      }

      /*stage('Deploy to prod' ) {
        when {
          expression { env.BRANCH_NAME == 'master' }
        }
        steps{
          container('helm-kubectl') {
            echo 'Start deploy helm chart'
            withKubeConfig([credentialsId: 'k8s-cf-preproduccion', serverUrl:"https://rancherpre.favorita.ec/k8s/clusters/c-mw5ln"]) {
              sh 'kubectl config current-context'
              sh "helm upgrade --install ${env.SHORT_NAME_APP} ./ci/helm --wait -n ${pipelineParams.namespace} --force"  // --create-namespace  + in helm 3.2
            }
          }
        }  
      }*/

      /*
      stage('Katalon test'){
        when {
          expression { env.BRANCH_NAME == 'develop' || env.BRANCH_NAME == 'master' }
        }
        steps {
          container('katalon') {
            script {
              sh "pwd"
              if (fileExists('end-to-end')) {
                sh "pwd"
                echo "test version exec: ${env.VERSION_TAG}"
                echo 'Start katalon'
                sh 'katalonc.sh -projectPath="./end-to-end" -browserType="Firefox" -retry=0 -statusDelay=15 -testSuitePath="Test Suites/test" -apiKey="ae74a191-2cb0-4cf0-a61e-1b1d4ffd5774" -sendMail=caguilar@ec.krugercorp.com'
              } else {
                echo 'No exists directory'
              }
            }
          }
        }
      }
*/

    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      aborted {
		    slackNotifyAborted(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}