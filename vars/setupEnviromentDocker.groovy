def call(Map config = [: ]) {
  container('docker') {
    script {
      env.DOCKER_REGISTRY = resolveDockerRegistry(env.VERSION_APP);
      env.DOCKER_REGISTRY_IMAGE = env.DOCKER_REGISTRY + '/' + env.NAME_APP.replace('@', '') + ':' + env.VERSION_APP
      env.DOCKER_REGISTRY_IMAGE_LATEST = env.DOCKER_REGISTRY + '/' + env.NAME_APP.replace('@', '') + ':latest'
      echo env.DOCKER_REGISTRY_IMAGE
      sh 'printenv'
    }
  }
}

String resolveDockerRegistry(String version) {
  String versionUpper = version.toUpperCase();
  if (versionUpper.contains("RELEASE") || versionUpper.contains("LATEST")) {
    return "cf1dckrls.cfavorita.net"
  } else {
    return "cf1dckspt.cfavorita.net"
  }
}