def call( Map config = [:] ){
    script{
        def isMaster = env.BRANCH_NAME == 'master'
        def isDevelop = env.BRANCH_NAME == 'develop'
        def snapshot = '-SNAPSHOT'
        def release = '-RELEASE'
        def isHotfix = env.BRANCH_NAME.startsWith('hotfix')
        def repoList = ALLOWED_REPOSITORIES.tokenize(',[] ')
        println repoList

        echo "BRANCH_NAME: ${env.BRANCH_NAME} \nVERSION_APP: ${env.VERSION_APP} \nGIT_REPO_NAME: ${env.GIT_REPO_NAME} \nALLOWED_REPOSITORIES: ${ALLOWED_REPOSITORIES}"
        
        if(
            (isMaster == true && env.VERSION_APP.endsWith(release)) ||
            (env.VERSION_APP.endsWith(snapshot) && ( isDevelop == true || isHotfix == true)) ||
            (repoList.contains(env.GIT_REPO_NAME) && env.BRANCH_NAME == 'release_candidate' && env.VERSION_APP.endsWith(release))
        ){
            echo "Branch and version is valid for publish"
            env.PUBLISH = true
        }else{
            print "Branch and version is not valid for publish \nRemember publish when \n1)master branch and release version \n2)develop/hotfix branch and snapshot version \n3) release_candidate branch and release version and your project have permissions"
            env.PUBLISH = false
        }
    }
}