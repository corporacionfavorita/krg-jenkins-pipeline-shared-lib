def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()
  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "java11-slave-${projectName}"

  properties([
    durabilityHint('PERFORMANCE_OPTIMIZED') 
  ])

  pipeline {
    agent {
      kubernetes {
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: java11-jnlp-slave
      jenkins/java11-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: gradle
      image: gradle:6.9.1-jdk11
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: docker
      image: docker:19.03.8
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock
    imagePullSecrets:
    - name: cf-regcred
  """
      }
    }

    environment {
      registryCredential = 'cf-nexus'
      NXSJAVA_CREDENTIAL = credentials('cf-nexus')
      NXSDOCKER_CREDENTIAL = credentials('cf-nexus')
      NXSHELM_CREDENTIAL = credentials('cf-nexus')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 30, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
        steps {
          checkCurrentBuilds()
        }
      }
      
      stage('Checkout code') {
        steps {
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
          setupEnviromentGit()
          container('git') {
            script {
              env.HAS_SUBPROJECTS = "false"
            }
          }
          setupEnviromentGradlew()

          setupEnviromentDocker()          
          
          gitFlowValidate()
        }
      }

      stage('Clean') {
        steps {
          container('gradle') {
            echo "Rama: ${env.BRANCH_NAME},  codigo de construccion: ${env.BUILD_ID} en ${env.JENKINS_URL}"
            echo "Iniciando limpieza"
            sh "./gradlew --console=plain clean -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
          }
        }
      }

      stage('Install dependencies - Build') {
        steps {
          echo "Iniciando construccion"
          container('gradle') {
            script {
              sh "./gradlew --console=plain build -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} -x test -x check --refresh-dependencies"
            }
          }
        }
      }

//      stage('Build Native Image') {
//        steps {
//          echo "Iniciando construccion"
//          container('gradle') {
//            script {
//              sh "gradle --console=plain build -Dquarkus.package.type=native -Dquarkus.native.container-build=true -Dmaven.repo.login=${NEXUS_CREDENTIAL_USR} -Dmaven.repo.password=${NEXUS_CREDENTIAL_PSW} -x test -x check --stacktrace"
//            }
//          }
//        }
//      }

      stage('Unit Test') {
        steps {
          container('gradle') {
            echo 'Test project'
            sh "./gradlew test --console=plain -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} -x check --stacktrace "
          }
        }
        post {
          always {
            junit allowEmptyResults: true, testResults: '**/test-results.xml'
          }
        }
      }

      /*stage('Native Test') {
        steps {
          container('gradle') {
          echo 'Test project'
            sh "gradle testNative --console=plain -Dmaven.repo.login=${NEXUS_CREDENTIAL_USR} -Dmaven.repo.password=${NEXUS_CREDENTIAL_PSW} -x check --stacktrace "
          }
        }
        post {
          always {
           junit allowEmptyResults: true, testResults: '**test-results.xml'               
          }
        }
      }*/

      stage('Jacoco Test Report') {
        steps {
          container('gradle') {
            echo 'Test report'
            sh "./gradlew --console=plain jacocoTestReport -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
          }
        }
      }

      stage('Sonarqube') {
        steps {
          sonarScanner10()
        }
      }

      stage('Code Quality - Security') {
        steps {
          container('gradle') {
            script {
              try {
                sh "./gradlew dependencyCheckAnalyze -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
              } catch(err) {
                unstable('Code Quality - Security failed!')
              }
            }
          }
        }
      }

      stage('Publish Artifact to Nexus') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          container('gradle') {
            echo "Publish Artifact to Nexus"
            sh "./gradlew upload -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
          }
        }
      }

      stage('Code Quality - Helm Lint') {
        steps {
          helmLint()
        }
      }

      stage('Build docker') {
        steps {
          dockerBuild()
        }
      }

      stage('Publish docker image') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          dockerPush()
        }
      }

      stage('Package helm chart') {
        steps {
          helmPackage()
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
          helmPush()
        }
      }
    }

    post {
      always {
        echo "Pipeline finalizado del repositorio de la rama ${env.BRANCH_NAME} con el codigo de construccion ${env.BUILD_ID} en ${env.JENKINS_URL}"
      }
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      changed {
        echo 'This will run only if the state of the Pipeline has changed'
        echo 'For example, if the Pipeline was previously failing but is now successful'
      }
    }
  }
}
