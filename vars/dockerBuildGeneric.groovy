def call(Map config = [: ]) {
  container('docker') {
    script {
      echo 'Init dockerBuild'
      echo 'Start build docker'
      sh 'docker version'
      // Docker login with repositories
      sh "docker login cf1dck.cfavorita.net --username=${NXSDOCKER_CREDENTIAL_USR} --password=${NXSDOCKER_CREDENTIAL_PSW}"
      sh "docker login cf1dckrls.cfavorita.net --username=${NXSDOCKER_CREDENTIAL_USR} --password=${NXSDOCKER_CREDENTIAL_PSW}"
      sh "docker login cf1dckspt.cfavorita.net --username=${NXSDOCKER_CREDENTIAL_USR} --password=${NXSDOCKER_CREDENTIAL_PSW}"
      if (env.HAS_SUBPROJECTS == "false") {
        sh "docker build --build-arg USER=${NXSDOCKER_CREDENTIAL_USR} --build-arg PASSWORD=${NXSDOCKER_CREDENTIAL_PSW} --network=host -t ${env.DOCKER_REGISTRY_IMAGE} --file ./Dockerfile ."
        //sh "docker tag ${env.DOCKER_REGISTRY_IMAGE} ${env.DOCKER_REGISTRY_IMAGE_LATEST}"
      } else {
        def projectDirs = env.PUBLISH_APP_PATHS.split('\n')
        def nameAppsList = env.NAME_APP_LIST.split('\n')
        for (i = 0; i < projectDirs.length; i++) {
          echo "Build docker sub ${projectDirs[i]}"
          def subProjectDir = "${projectDirs[i]}"
          def subNameApp = "${nameAppsList[i]}"
          def dockerRegistryImage = env.DOCKER_REGISTRY + '/' + subNameApp.replace('@', '') + ':' + env.VERSION_APP
          //def dockerRegistryImageLastest = env.DOCKER_REGISTRY + '/' + subNameApp.replace('@', '') + ':latest'
          sh "docker build --build-arg USER=${NXSDOCKER_CREDENTIAL_USR} --build-arg PASSWORD=${NXSDOCKER_CREDENTIAL_PSW} --network=host -t ${dockerRegistryImage} --file ./${subProjectDir}/Dockerfile ."
          //sh "docker tag ${dockerRegistryImage} ${dockerRegistryImageLastest}"
        }
      }
    }
  }
}