import ec.com.smx.Constants
def call(body) {
  def pipelineParams = [: ]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = pipelineParams
  body()
  def projectName = env.JOB_NAME.split('/')[0]
  def labelgenerated = "java8-slave-${projectName}"

  properties([
    durabilityHint('PERFORMANCE_OPTIMIZED')
  ])

  pipeline {
    agent {
      kubernetes {
        label labelgenerated
        yaml """
  apiVersion: v1
  kind: Pod
  metadata:
    labels:
      jenkins-agent: java8-jnlp-slave
      jenkins/java8-slave: true
  spec:
    containers:
    - name: jnlp
      image: jenkins/inbound-agent:jdk11
      imagePullPolicy: IfNotPresent
      livenessProbe:
        exec:
          command:
          - uname
          - -a
        initialDelaySeconds: 30
        timeoutSeconds: 1
        failureThreshold: 1
    - name: git
      image: alpine/git:v2.24.3
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: sonar-scanner
      image: sonarsource/sonar-scanner-cli:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
    - name: gradle
      image: cf1dck.cfavorita.net/ec.com.smx.jenkins/gradle-agent:1.0.1-RELEASE
      imagePullPolicy: IfNotPresent
      command:
      - cat    
      env:
      - name: GRADLE_OPTS
        value: -Xms2g -Xmx4g -XX:MaxHeapSize=3g -Dfile.encoding=UTF-8 -XX:+UseG1GC -XX:+HeapDumpOnOutOfMemoryError
      tty: true     
    - name: docker
      image: docker:19.03.8
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock
    - name: helm-kubectl
      image: dtzar/helm-kubectl:latest
      imagePullPolicy: IfNotPresent
      command:
      - cat
      tty: true
      volumeMounts:
      - mountPath: /var/run/docker.sock
        name: docker-sock    
    volumes:
      - name: docker-sock
        hostPath:
          path: /var/run/docker.sock      
    imagePullSecrets:
    - name: regdock
    - name: cf-regcred    
  """
      }
    }

    environment {
      registryCredential = 'cf-nexus'
      NXSJAVA_CREDENTIAL = credentials('cf-nexus')
      NXSDOCKER_CREDENTIAL = credentials('cf-nexus')
      NXSHELM_CREDENTIAL = credentials('cf-nexus')
    }

    triggers {
      bitbucketPush()
    }

    options {
      disableConcurrentBuilds()
      skipDefaultCheckout(true)
      buildDiscarder(logRotator(numToKeepStr: '10'))
      timeout(time: 30, unit: 'MINUTES')
    }

    stages {
      stage('Check Concurrent Builds') {
        steps {
            script {
                    env.CURRENT_STAGE = Constants.STAGES.CHECK_CURRENT_BUILDS
                }
          checkCurrentBuilds()
        }
      }
      
      stage('Checkout code') {
        steps {
            script {
                env.CURRENT_STAGE = Constants.STAGES.CHECKOUT_CODE
            }
          echo "Cambios en la rama ${env.BRANCH_NAME}\n"
          echo 'Checkout code'
          checkout scm
        }
      }

      stage('Set up environment') {
        steps {
              script {
                env.CURRENT_STAGE = Constants.STAGES.SETUP_ENVIRONMENT
            }
          setupEnviromentGit()

          setupEnviromentGradle()

          setupEnviromentDocker()
          
          gitFlowValidate()
        }
      }

      stage('Clean') {
        steps {
            script {
                env.CURRENT_STAGE = Constants.STAGES.CLEAN
            }
          container('gradle') {
            echo "Rama: ${env.BRANCH_NAME},  codigo de construccion: ${env.BUILD_ID} en ${env.JENKINS_URL}"
            echo "Iniciando limpieza"
//            sh 'rsync -a /home/gradle/data/ /home/gradle/.gradle'
            sh 'gradle --console=plain clean -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}'
          }
        }
      }

      stage('Refresh dependencies - Build') {
        steps {
          echo "Iniciando construccion"
          script {
                env.CURRENT_STAGE = Constants.STAGES.BUILD
            }
          container('gradle') {
            script {
              sh "gradle build --refresh-dependencies copyLombokToLibOut -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} -x test -x pmdMain -x check --quiet --build-cache --parallel"
//              sh 'rsync -a /home/gradle/.gradle/caches/ /home/gradle/data/caches'
            }
          }
        }
      }

      stage('Test/QC') {
        failFast true
        parallel {
          stage('Code Quality - Sonarqube') {
            stages {
              stage('Unit Test') {                 
                when {
                  expression {
                    pipelineParams.omitTest == null || pipelineParams.omitTest == false
                  }
                }
                steps {
                  container('gradle') {
                    script {
                      env.CURRENT_STAGE = Constants.STAGES.UNIT_TEST
                      echo 'Test project'
                      try {
                        sh "gradle test --console=plain -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW} -xcheck -x pmdMain --stacktrace "
                      } catch(err) {
                        unstable('Unit Test - Unit Test failed!')
                      }
                    }
                  }
                }
                post {
                  always {
                    junit allowEmptyResults: true,
                    testResults: '**/test-results.xml'
                  }
                }
              }

              stage('Jacoco Test Report') {
                steps {
                  container('gradle') {
                      script {
                              env.CURRENT_STAGE = Constants.STAGES.JACOCO_TEST
                             }      
                    echo 'Test report'
                    sh "gradle --console=plain jacocoTestReport -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
                  }
                }
              }

              stage('Sonarqube') {
                steps {
                      script {
                               env.CURRENT_STAGE = Constants.STAGES.SONARQUBE
                            }
                  sonarScanner()
                }
              }
            }
          }

          stage('Code Quality - helm lint') {
            steps {
                script {
                env.CURRENT_STAGE = Constants.STAGES.HELM_LINT
            }
              helmLint()
            }
          }
        }
      }

      stage('Code Quality - Security') {
        steps {
            script {
                env.CURRENT_STAGE = Constants.STAGES.SECURITY
            }
          container('gradle') {
            script {  
              try {
                sh 'gradle dependencyCheckAnalyze -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}'
              } catch(err) {
                unstable('Code Quality - Security failed!')
              }
            }
          }
        }
      }

      stage('Publish Artifact to Nexus') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
            script {
                env.CURRENT_STAGE = Constants.STAGES.PUBLISH_DEPENDENCY
            }
          container('gradle') {
            echo "Publish Artifact to Nexus"
            sh "gradle upload -Dmaven.repo.login=${NXSJAVA_CREDENTIAL_USR} -Dmaven.repo.password=${NXSJAVA_CREDENTIAL_PSW}"
          }
        }
      }

      stage('Build docker') {
        steps {
            script {
                env.CURRENT_STAGE = Constants.STAGES.DOCKER_BUILD
            }
          dockerBuild()
        }
      }

      stage('Publish docker image') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
              script {
                env.CURRENT_STAGE = Constants.STAGES.PUBLISH_DOCKER
            }
          dockerPush()
        }
      }

      stage('Package helm chart') {
        steps {
              script {
                env.CURRENT_STAGE = Constants.STAGES.PACKAGE_HELM
            }
          helmPackage()
        }
      }

      stage('Publish helm chart') {
        when {
          expression {
            env.PUBLISH.toBoolean() == true
          }
        }
        steps {
              script {
                env.CURRENT_STAGE = Constants.STAGES.PUBLISH_HELM
            }
          helmPush()
        }
      }

      stage('Delivery to qa') {
        when {
          expression {
            (env.PUBLISH.toBoolean() == true) && pipelineParams.enableContinuosDelivery == true
          }
        }
        options {
          timeout(time: 300, unit: 'SECONDS') 
        }                
        steps {
          helmDeploy(slackNotificationChannel: pipelineParams.slackNotificationChannel, namespace: pipelineParams.namespace)
        }
      }
    }

    post {
      success {
        slackNotifySuccess(pipelineParams.slackNotificationChannel)
      }
      failure {
        suggestion()
        slackNotifyFailure(pipelineParams.slackNotificationChannel)
      }
      unstable {
        slackNotifyUnstable(pipelineParams.slackNotificationChannel)
      }
      aborted {
		    slackNotifyAborted(pipelineParams.slackNotificationChannel)
      }
    }
  }
}