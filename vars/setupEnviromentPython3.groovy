def call(Map config = [: ]) {
  container('python3') {
    script {
      env.VERSION_APP = sh(script: "python3 setup.py --version |tail -1", returnStdout: true).trim()
      env.NAME_APP = sh(script: "python3 setup.py --name |tail -1", returnStdout: true).trim()
      env.SHORT_NAME_APP = env.NAME_APP.substring(env.NAME_APP.lastIndexOf('/') + 1, env.NAME_APP.length())
      env.HAS_SUBPROJECTS = "false"
      sh 'printenv'
    }
  }
}