def call(String slackNotificationChannel = 'devops') {
  echo 'La linea de construccion fue cancelada'
  slackSend(
    iconEmoji: ":robot_face",
    channel: slackNotificationChannel,
    color: "#FF6B21",
    message: ":worried::interrobang: *CONSTRUCCIÓN CANCELADA* en construcción ${currentBuild.fullDisplayName}\n *Repositorio:* `${env.GIT_REPO_NAME}`\n *Rama:* `${env.BRANCH_NAME}`\n *Autor:* ${env.GIT_USER_NAME} [${env.GIT_USER_EMAIL}]\n *Commit:* ${env.GIT_COMMIT}\n *Construcción:* ${env.BUILD_NUMBER}| <${env.BUILD_URL}|Revisar>"
  )
}
