package ec.com.smx

class Messages {
  public static final def CHECK_CURRENT_BUILDS = ['Los errores se presentan en la etapa de: Check Concurrent Builds',
  'Recuerda que al construir varias ramas a la vez te aparecerá este error para evitar saturación en el servidor.'
  ,'Al realizar la verificación anterior y estar seguro que no están construyendo más ramas, vuelve a construir y se resolverá el problema.']

  public static final def CHECKOUT_CODE = ['Los errores se presentan en la etapa de: Check out Code',
  'Este error sucede cuando el proyecto presenta problemas en la conexión con el repositorio (bitbucket).',
  'Verificar si bitbucket se encuentra con problemas o esté abajo el servicio.']

  public static final def SETUP_ENVIRONMENT = ['Los errores se presentan en la etapa de: Set Up Environment',
  'Este error sucede cuando presenta algún problema en la configuración del entorno.','La razón más común de que suceda es cuando en la construcción no reconoce unos scripts de gradle.',
  'Para solucionarlo, debemos ir al build.gradle principal del proyecto y añadir el archivo común que contiene los script para reconocer los comandos gradle.',
  'Accede a la siguiente guía de referencia https://www.krugle.ec/pages/viewpage.action?pageId=109249960 para encontrar información relacionada.']

  public static final def CLEAN = ['Los errores se presentan en la etapa de: Clean','Error que se presenta al momento de ejecutar un clean en el proyecto.',
  'Verificar localmente ejecutando el comando gradle clean y comprobar el error.']

  public static final def BUILD = ['Los errores se presentan en la etapa de: Refresh dependencies - Build',
  'Este error sucede cuando presenta algún problema en la construcción o descarga de dependencias en el proyecto.',
  'Una de las razones que no se pueda construir, es debido a las reglas PMD.',
  'Se recomienda ejecutar el comando gradle clean build de forma local y verificar cuales son las reglas que no se están cumpliendo.',
  'Otra razón es - Presentar error al descargar las dependencias que el proyecto utiliza. Se recomienda revisar cuales son las librerías que no es posible descargar.',
  'Recordar que el Jenkins no maneja cache, es decir que las librerías se descargarán al momento de la construcción.',
  'Suele ocurrir que al ejecutar de forma local funciona ya que las dependencias han sido descargadas previamente (para esa prueba se recomienda borrar la caché).']

  public static final def UNIT_TEST = ['Los errores se presentan en la etapa de: Unit Test','Sucede cuando presenta algún problema en la ejecución de los test unitarios del proyecto.',
  'Se recomienda ejecutar el comando gradle test de forma local y verificar el inconveniente.',
  'Verificar si el proyecto tiene importadas las librerías para ejecución de tests en el archivo build.gradle del paquete donde se encuentran los objetos a testear.',
  'Accede a la siguiente guía de referencia https://www.krugle.ec/display/CWC/Test+Unitarios+Proyectos+Springboot para encontrar información relacionada.']

  public static final def INTEGRATION_TEST = ['Los errores se presentan en la etapa de: Integration Test',
  'Sucede cuando presenta algún problema en la ejecución de los test de integración del proyecto.',
  'Se recomienda ejecutar el comando gradle integrationTest jacocoTestReport de forma local y verificar el inconveniente.',
  'Verificar si el proyecto tiene importadas las librerías para ejecución de tests en el archivo build.gradle del paquete donde se encuentran los objetos a testear.',
  'Accede a la siguiente guía de referencia https://www.krugle.ec/display/CWC/Test+Unitarios+Proyectos+Springboot para encontrar información relacionada.']

  public static final def JACOCO_TEST = ['Los errores se presentan en la etapa de: Jacoco Test Report',
  'Sucede cuando presenta algún problema al generar el reporte de test jacoco.',
  'Se recomienda ejecutar el comando gradle jacocoTestReport de forma local y verificar el inconveniente.',
  'Verificar si el proyecto tiene importadas la librería de jacocoTestReport en el archivo build.gradle del paquete donde se encuentran los objetos a testear.',
  'Accede a la siguiente guía de referencia https://www.krugle.ec/display/CWC/Test+Unitarios+Proyectos+Springboot para encontrar información relacionada.'] 

  public static final def SONARQUBE = ['Los errores se presentan en la etapa de: Sonarqube',
  'Sucede cuando presenta algún problema en el análisis de la calidad de código del proyecto.',
  'Se recomienda verificar que exista conexión al servidor del sonarqube.',
  'En el caso que se presente un error relacionado a una conexión fallida comunicar al administrador.',
  'En caso de problemas con el sonar-project.properties del proyecto, se recomienda revisarlo comparando con el proyecto fwk-samples-root.'] 

  public static final def PUBLISH_DEPENDENCY = ['Los errores se presentan en la etapa de: Publish Artifact to Nexus',
  'Sucede cuando presenta algún problema al momento de subir los artefactos a nexus.',
  'Se debe tener presente que los artefactos del tipo RELEASE no se pueden sobreescribir, solamente se pueden subir una vez.',
  'Únicamente los artefactos del tipo SNAPSHOT se sobreescriben.',
  'Verificar si el nexus no se encuentra en línea, en tal caso comunicarlo con el administrador.']

  public static final def SECURITY = ['Los errores se presentan en la etapa de: Code Quality - Security',
  'Sucede cuando presenta algún problema en el análisis de vulnerabilidades en las dependencias del proyecto.',
  'Se recomienda verificar que su proyecto tenga importada la dependencia dependencyCheckAnalyze.',
  'Ejecutar el comando gradle dependencyCheckAnalyze localmente y verificar que se ejecute correctamente.',
  'Accede a la siguiente guía de referencia https://www.krugle.ec/display/CDN/OWASP+Springboot+-+CI para encontrar información relacionada.']

  public static final def HELM_LINT = ['Los errores se presentan en la etapa de: Code Quality - Helm Lint',
  'Sucede cuando presenta algún problema al pasar el código por una serie de tests para verificar que el chart esté bien formado.',
  'Se recomienda ejecutar el comando helm lint de forma local y verificar el inconveniente.',
  'Usualmente presenta un problema al no poner la especificación de *manteiners* acerca del nombre y correo del desarrollador.',
  'Revisar el proyecto samples en el Chart.yaml para comparar la estructura.'] 

  public static final def DOCKER_BUILD = ['Los errores se presentan en la etapa de: Build docker',
  'Sucede cuando presenta algún problema al construir la imagen docker del proyecto.',
  'Se recomienda construir la imagen de forma local para revisar en que paso está fallando.',
  'Los pasos se encuentran el el archivo readme de la carpeta ci. Es necesario tener instalado docker en nuestra máquina.',
  'En ocasiones algunos comandos no están ingresados de forma correcta en el archivo DockerfIle o los archivos que se desea copiar no coinciden con el nombre ni ubicación específica']

  public static final def PUBLISH_DOCKER = ['Los errores se presentan en la etapa de: Publish docker image',
  'Sucede cuando presenta algún problema al momento de subir la imagen docker al repositorio de nexus.',
  'Se debe tener presente que las imagenes docker del tipo RELEASE no se pueden sobreescribir, solamente se pueden subir una vez.',
  'Únicamente los artefactos del tipo SNAPSHOT se sobreescriben.','Verificar si el nexus no se encuentra en línea, en tal caso comunicarlo con el administrador.']  

  public static final def PACKAGE_HELM = ['Los errores se presentan en la etapa de: Package helm chart',
  'Sucede cuando al momento de empaquetar los archivos de la carpeta helm no están correctos.',
  'Se puede probar de forma local que la carpeta ci este formada correctamente, para ello es importante que instalemos helm chart en nuestra máquina.',
  'Helm chart ofrece un comando para verificar dicha formación helm lint (https://helm.sh/docs/helm/helm_lint/)']

  public static final def PUBLISH_HELM = ['Los errores se presentan en la etapa de: Publish helm chart',
  'Sucede cuando presenta algún problema al momento de subir el helm chart al repositorio de nexus.',
  'Se debe tener presente que los chart del tipo RELEASE no se pueden sobreescribir, solamente se pueden subir una vez.',
  'Únicamente los chart del tipo SNAPSHOT se sobreescriben.','Verificar si el nexus no se encuentra en línea, en tal caso comunicarlo con el administrador.'] 

  public static final def INSTALL_ANG = ['Los errores se presentan en la etapa de: Install dependencies',
  'Sucede cuando presenta algún problema en la instalación de dependencias del proyecto.',
  'Se recomienda ejecutar el comando npm install de forma local y verificar el inconveniente.',
  'Se recomienda revisar cuales son las dependencias que no es posible instalar.',
  'Recordar que el Jenkins no maneja caché, es decir que las dependencias se descargarán al momento de la construcción.',
  'Suele ocurrir que al ejecutar de forma local funciona ya que las dependencias han sido descargadas previamente (para esa prueba se recomienda borrar la caché).']

  public static final def SETUP_ENVIRONMENT_ANG = ['Los errores se presentan en la etapa de: Set Up Environment',
  'Este error sucede cuando presenta algún problema en la configuración del entorno.',
  'Una posible razón es que no se reconoce unos scripts npm specificados como version o name en el archivo package.json.'] 

  public static final def UNIT_TEST_ANG = ['Los errores se presentan en la etapa de: Unit Test',
  'Sucede cuando presenta algún problema en la ejecución de los test unitarios del proyecto.',
  'Se recomienda ejecutar el comando npm run-script test:unit de forma local y verificar el inconveniente.',
  'Verificar si el proyecto tiene importadas las dependencias para ejecución de tests, en el archivo package.json.',
  'Accede a la siguiente guía de referencia https://www.krugle.ec/display/CWC/Test+Unitarios+Angular para encontrar información relacionada.'] 

  public static final def INTEGRATION_TEST_ANG = ['Los errores se presentan en la etapa de: Integration Test',
  'Sucede cuando presenta algún problema en la ejecución de los test de integración del proyecto.',
  'Se recomienda ejecutar el comando npm run-script test:integration de forma local y verificar el inconveniente.',
  'Revisar si dentro del archivo package.json tenga el script test:integration'] 

  public static final def QUALITY_LINT_ANG = ['Los errores se presentan en la etapa de: Code Quality - Lint',
  'Sucede cuando presenta algún problema evaluando las métricas Eslint que se deben cumplir del proyecto.',
  'Ejecutar el comando npm run-script lint:ci en el proyecto de forma local, y revisar el archivo que se crea llamado checkstyle-result.xml',
  'En el archivo referido se encontrarán las reglas de lint que se deben resolver.',
  'Revisar si el proyecto ha presentado daños en algún archivo como el angular.json o los tsconfig, debido a que al construir puede presentar problemas.',
  'Accede a la siguiente guía de referencia https://www.krugle.ec/display/CDN/CDN+-+ESlint para encontrar información relacionada.']

  public static final def BUILD_ANG = ['Los errores se presentan en la etapa de: Build',
  'Sucede cuando presenta algún problema en la construcción del proyecto angular.',
  'Se recomienda ejecutar el comando npm run-script build:ci de forma local y verificar si no existe errores en algún archivo.',
  ' Revisar el archivo angular.json y verificar el campo maximumError, por si necesite extender el tamaño.']

  public static final def SECURITY_ANG = ['Los errores se presentan en la etapa de: Code Quality - Security',
  'Sucede cuando presenta algún problema en el análisis de vulnerabilidades en las dependencias del proyecto.',
  'Con el comando npm run-script test:security, podemos ejecutarlo localmente, y resolver los problemas que surgen  de vulnerabilidades de dependencias.',
  'Revisar si se tiene las dependencias importadas correctamente en el package.json.']
  
}