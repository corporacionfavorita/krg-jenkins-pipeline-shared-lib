package ec.com.smx

class Constants {
  public static final String NPM_BUILD14 = "node14"
  public static final String NPM_BUILD20 = "node20"
  public static final String GRADLE_BUILD4 = "gradle4"
  public static final String GRADLE_BUILD8 = "gradle8"
  public static final String PYTHON_BUILD = "python3"
  public static final String NPM_BUILD = "npm"
  public static final String GRADLE_BUILD = "gradle"
  public static final String FAIL_SUGGESTION = "FAILURE! -> SUGERENCIA "
  public static final enum STAGES {
    CHECK_CURRENT_BUILDS,
    CHECKOUT_CODE,
    SETUP_ENVIRONMENT, 
    CLEAN,
    BUILD,
    UNIT_TEST,
    INTEGRATION_TEST, 
    JACOCO_TEST,
    SONARQUBE,
    PUBLISH_DEPENDENCY,
    SECURITY,
    HELM_LINT,
    DOCKER_BUILD,
    PUBLISH_DOCKER,
    PACKAGE_HELM,
    PUBLISH_HELM,
    SETUP_ENVIRONMENT_ANG,
    INSTALL_ANG,
    UNIT_TEST_ANG,
    INTEGRATION_TEST_ANG,
    QUALITY_LINT_ANG,
    BUILD_ANG,
    SECURITY_ANG
  }
}